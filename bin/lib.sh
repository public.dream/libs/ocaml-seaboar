#!/usr/bin/env bash

set -eu
set -o pipefail

find-it () {
  local ret=$1; shift
  local cmd=("$@")
  cmd-print "${cmd[@]}"
  while read -r -d $'\0' fn; do
    eval "$ret+=(\"\$fn\")"
  done < <(${cmd[@]})
}

