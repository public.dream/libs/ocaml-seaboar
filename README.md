# dream-encoding

This library provides OCaml functions for encoding and decoding arbitrary OCaml
data structures as binary strings in the [CBOR](https://cbor.io) format.

## Usage

    opam install --deps-only --with-test .
    # --- run tests
    dune test [-f]
    # --- see test.ml to understand how to use this package to encode and decode
    your data.

### Nix Flakes

    # --- build all the things
    nix build
    # --- get a develop shell
    nix develop

### Nix Legacy

    # --- build all the things
    nix-build
    # --- get a develop shell
    nix-shell

## Overview

Encoding is implemented by piggybacking off of the
[cbor](https://opam.ocaml.org/packages/cbor/) library.

Decoding is achieved using custom parsers implemented using
[Angstrom](https://opam.ocaml.org/packages/angstrom/). Our package exports a
number of combinators in the monadic parsing style for expressively decoding
arbitrary OCaml data.

The bulk of the code in the library is oriented towards providing useful
combinators for decoding.

## Design goals

The library is useful in three main ways. In all cases, consult the code in
test.ml to see how to achieve the various tasks.

First, when encoding and decoding arbitrary (and arbitrarily complex) OCaml data
structures, when you does not care about the layout of the binary data in the
CBOR representation. In this case the expectation is that the data will be
decoded using this library again.

At the moment this requires some hand-written code which is completely
predictable and could in the future be generated by for example a ppx deriver.
Generating the code would also provide a guarantee that the encoder and decoder
actually match. The advantage to this approach is that a single function could
be called to encode an arbitrary OCaml data structure and another one to decode
it.

The second case is similar to the first, but is when you do wish to produce a
specific layout in the CBOR string. For example, you could be designing a format
for interoperability of components, meaning that the string may later be decoded
by a different library or language. This requires more hand-written code and is
not easy to automate. One must also take care to ensure that the encoder and
decoder actually match.

The third case is what we call “generic” decoding, which is for decoding an
arbitrary and unknown CBOR string. This is useful when you doesn't know what
kind of data structure a CBOR string should produce, and allows you to parse it
anyway and then inspect it or process it further if parsing succeeds. The
resultant OCaml structure is built using arbitrarily nested groups of
polymorphic variant tags — for example a string containing an array of arrays of
integers may produce something like:

```ocaml
- : [> `Array of [> `Array of [> `Int of int ] list ] list ]
`Array [`Array [`Int 1; `Int 2]; `Array [`Int 3; `Int 4]]
```

If you know what kind of data you expect, you should probably use one of the
specific forms instead of the generic form, or else you'll have to loop and map
and fold through the generic structure manually.

## Improvements

The parser uses in some cases a naïve but accurate approach of trying several
Angstrom parsers in a row. This can be greatly improved by more faithfully
following the CBOR algorithm of first parsing the first byte and then deciding
what to do afterwards. The expectation is that this will lead to an order of
magnitutde improvement in some cases.
