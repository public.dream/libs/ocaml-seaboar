(*
 * SPDX-FileCopyrightText: 2021 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module T = Alcotest

module LD = Seaboar.Decode
module LE = Seaboar.Encode

module TA = Decode_test.M
module TE = Encode_test
module TT = Types
module TTA = Types.Animal

module UT = Util_test

type angstrom_decode_result = (TTA.animal_list, string) result

let ((<|>), (<$>)) = Seaboar.Decode.Angstrom.((<|>), (<$>))

(* --- for debugging *)
let print_data = TTA.show_animal_list %> print_endline

(* e.g. "0x6162..." -> "\x61\x62..." *)
let to_bytes =
  let f = Fmt.str "0x%s" %> int_of_string %> Char.chr %> BatString.of_char in
  UT.str_chunk 2
  %> BatList.drop 1
  %> List.map f
  %> BatString.join ""

(* e.g. "\x61\x62..." -> "0x6162..." *)
let of_bytes =
  let f acc x = acc ^ Fmt.str "%02x" (Char.code x) in
  BatString.fold_left f ""
  %> Fmt.str "0x%s"

let range x y = BatList.range x `To y

(* not efficient *)
let pp_list_int ppf = List.map string_of_int
  %> BatString.join ", "
  %> Fmt.pf ppf "[%s]"

let pp_list_a_yada_yada pp_int' ppf xs =
  let s = List.hd xs in
  let t = BatList.last xs in
  Fmt.pf ppf "[%a … %a]" pp_int' s pp_int' t

let pp_list_int_yada_yada =
  pp_list_a_yada_yada Fmt.int

let pp_list_float_yada_yada =
  pp_list_a_yada_yada Fmt.float

let make_animal_meta () =
    let date' = UT.epoch () in
    let shiny' = UT.random_bool () in
    TTA.mk_meta date' shiny'

let data1 : TTA.animal_list =
    let open TTA in
    [ mk_adult_elephant (make_animal_meta ()) 10 (Color1 Turquoise);
      mk_adult_elephant_multiple_speeds (make_animal_meta ()) [20; 21; 22] (Color1 RubyRed);
      mk_baby_elephant (make_animal_meta ()) 5 (Color1 RubyRed);
      mk_adult_elephant_multiple_speeds (make_animal_meta ()) [20; 21; 22] (Color2 `Aqua);
      mk_baby_elephant (make_animal_meta ()) 5 (Color2 (`Golden (Brightness 1.0, Contrast 2.0)));
      mk_wombot (make_animal_meta ()) 10 ]

let data2 : TTA.animal_list =
    let open TTA in
    [ mk_adult_elephant (make_animal_meta ()) 20 (Color1 Turquoise);
      mk_adult_elephant (make_animal_meta ()) 20 (Color1 RubyRed);
      mk_baby_elephant (make_animal_meta ()) 5 (Color1 RubyRed);
      mk_wombot (make_animal_meta ()) 10 ]

let eq = T.of_pp TTA.pp_animal_list
let ne = T.neg eq

let check_result_ok_and_pred pred s r1 r2 = match (r1, r2) with
  | Ok v1, Ok v2 -> T.check pred s v1 v2
  | _, _ -> T.fail "results not both ok"

let check_result_any_ok desc' : ('a, 'b) result -> unit
  = function
    | Ok _ -> T.check T.int desc' 1 1
    | Error _ -> T.check T.int desc' 1 2

let check_result_any_error desc' : ('a, 'b) result -> unit
  = function
    | Ok _ -> T.check T.int desc' 1 2
    | Error _ -> T.check T.int desc' 1 1

let check_angstrom_result_ok_and_eq
  : string -> angstrom_decode_result -> angstrom_decode_result -> unit
  = check_result_ok_and_pred eq
let check_angstrom_result_ok_and_ne
  : string -> angstrom_decode_result -> angstrom_decode_result -> unit
  = check_result_ok_and_pred ne

let angstrom_tests_int = [
  (* can produce huge output files on disk *)
  T.test_case "encode+decode any int" `Quick begin fun () ->
    let test data' =
      let roundtrip = data'
      |> LE.encode LE.int
      |> LD.(run_parser_string int) in
      let desc' = Fmt.str "%d" data' in
      T.check T.(result int string) desc' (Ok data') roundtrip in
    (* @todo how to test big ints? 4294967295 - 18446744073709551615 *)
    for i = -100 to 100 do
      test i
    done;
    for i = -4294967296 to -4294967294 do
      test i
    done;
  end;
  T.test_case "encode+decode specific int" `Quick begin fun () ->
    let test data' =
      let roundtrip_good = data'
      |> LE.encode LE.int
      |> LD.(run_parser_string (int' data')) in
      let roundtrip_bad = data'
      |> LE.encode LE.int
      |> LD.(run_parser_string (int' (data' + 1))) in
      let desc' = Fmt.str "%d" data' in
      T.check T.(result int string) desc' (Ok data') roundtrip_good;
      T.check T.(result int pass) desc' (Error "<any error>") roundtrip_bad;
    in for i = -20 to 20 do test i done
  end;
  T.test_case "decode int (explicit encoding)" `Quick begin fun () ->
    let test (expected', cbor') =
      let decoded' : (int, string) result = cbor'
      |> to_bytes
      |> LD.(run_parser_string int) in
      let desc' = Fmt.str "decode %s" cbor' in
      T.check T.(result int string) desc' (Ok expected') decoded' in
    List.iter test [
      0, "0x00";
      0, "0x1800";
      0, "0x190000";
      0, "0x1a00000000";
      0, "0x1b0000000000000000";
      1, "0x01";
      1, "0x1801";
      1, "0x190001";
      1, "0x1a00000001";
      1, "0x1b0000000000000001";
    ]
  end;
]

let float = T.float epsilon_float

let angstrom_tests_float = [
  T.test_case "encode+decode any float" `Quick begin fun () ->
    let test data' =
      let roundtrip = data'
      |> LE.encode LE.float
      |> LD.(run_parser_string float) in
      let desc' = Fmt.str "%f" data' in
      T.check T.(result (float epsilon_float) string) desc' (Ok data') roundtrip in
    let () = [-13.590; -2734927349234.2834823048234; 0.0; 1.1; 1.10000; 1.150; 12.9; infinity; neg_infinity; nan] |> List.iter test in
    let () = BatList.range 1 `To 1000
    |> List.map (float_of_int %> (/.) 1.)
    |> List.iter test in
    ()
  end;
  T.test_case "encode+decode specific float" `Quick begin fun () ->
    let test data' =
      let roundtrip_good = data'
      |> LE.encode LE.float
      |> LD.(run_parser_string (float' data')) in
      let roundtrip_bad = data'
      |> LE.encode LE.float
      |> LD.(run_parser_string (float' (data' +. 4.5))) in
      let desc' = Fmt.str "%f" data' in
      T.check T.(result (float epsilon_float) string) desc' (Ok data') roundtrip_good;
      T.check T.(result (float epsilon_float) pass) desc' (Error "<any error>") roundtrip_bad;
    in List.iter test [
      -20.0; 0.0; 10.5; 234234.101020;
    ]
  end;
  T.test_case "decode float (explicit encoding)" `Quick begin fun () ->
    let test (expected', cbor', epsilon') =
      let decoded' : (float, string) result = cbor'
      |> to_bytes
      |> LD.(run_parser_string float) in
      let desc' = Fmt.str "decode %s" cbor' in
      T.check T.(result (float epsilon') string) desc' (Ok expected') decoded' in
    List.iter test [
      (* half-precision *)
      0., "0xf90000", epsilon_float;
      0.000000059604645, "0xf90001", epsilon_float *. 2.;
      0., "0xf98000", epsilon_float;
      65504., "0xf97bff", epsilon_float;
      1., "0xf93c00", epsilon_float;
      infinity, "0xf97c00", epsilon_float;
      neg_infinity, "0xf9fc00", epsilon_float;

      (* single-precision *)
      100000.0, "0xfa47c35000", epsilon_float;

      (* double-precision *)
      20.3, "0xfb40344ccccccccccd", epsilon_float;
    ]
  end;
]

let angstrom_tests_bool = [
  T.test_case "encode+decode bool" `Quick begin fun () ->
    let test data' =
      let roundtrip = data'
      |> LE.encode LE.bool
      |> LD.(run_parser_string bool) in
      let desc' = Fmt.str "%b" data' in
      T.check T.(result bool string) desc' (Ok data') roundtrip in
    [true; false] |> List.iter test
  end;
  T.test_case "encode+decode specific bool" `Quick begin fun () ->
    let test (data', decoder', decoded', testable') =
      let roundtrip = data'
      |> LE.encode LE.bool
      |> LD.(run_parser_string decoder') in
      let desc' = Fmt.str "%b" data' in
      T.check T.(result testable' string) desc' (Ok decoded') roundtrip in
    [true, LD.true', (), T.unit; false, LD.false', (), T.unit] |> List.iter test;
    [true, LD.true_as_true, true, T.bool; false, LD.false_as_false, false, T.bool] |> List.iter test;
  end;
  T.test_case "encode+decode specific bool, should fail" `Quick begin fun () ->
    let test (data', decoder') =
      let roundtrip = data'
      |> LE.encode LE.bool
      |> LD.(run_parser_string decoder') in
      let desc' = Fmt.str "%b" data' in
      T.check T.(result bool pass) desc' (Error "<any error>") roundtrip in
    [false, LD.true_as_true; true, LD.false_as_false] |> List.iter test;
  end;
]

let angstrom_tests_byte_string = [
  T.test_case "encode+decode any byte string" `Quick begin fun () ->
    let test data' =
      let roundtrip = data'
      |> LE.encode LE.bytes
      |> LD.(run_parser_string byte_string) in
      let desc' = Fmt.str "%s" data' in
      T.check T.(result string string) desc' (Ok data') roundtrip in
    let () = ["hello"; "\x23\xff\x12\x00"; ""] |> List.iter test in
    (* @todo test longer strings? *)
    let f = BatString.repeat "৯" %> test in
    List.iter f [
      0; 1; 2; 3; 6; 9; 18; 36; 72; 144; 300;
      600; 900; 1800; 3600; 7200; 15000; 30000;
      60000; 120000; 240000; 1000000; 2000000; 10000000;
    ]
  end;
  T.test_case "encode+decode specific byte string" `Quick begin fun () ->
    let test data' =
      let roundtrip_good = data'
      |> LE.encode LE.bytes
      |> LD.(run_parser_string (byte_string' data')) in
      let roundtrip_bad = data'
      |> LE.encode LE.bytes
      |> LD.(run_parser_string (byte_string' "\x01\x02")) in
      let desc' = Fmt.str "%s" data' in
      T.check T.(result string string) desc' (Ok data') roundtrip_good;
      T.check T.(result string pass) desc' (Error "<any error>") roundtrip_bad;
    in
    let () = ["hello"; "\x23\xff\x12\x00"; ""] |> List.iter test in
    let f = BatString.repeat "৯" %> test in
    List.iter f [
      0; 1; 2; 3; 6; 9; 18; 36; 72; 144; 300;
      600; 900; 1800; 3600; 7200; 15000; 30000;
      60000; 120000; 240000; 1000000; 2000000; 10000000;
    ]
  end;
  T.test_case "encode+decode byte string (indefinite)" `Quick begin fun () ->
    let test (expected', cbor') =
      let decoded' = cbor'
      |> to_bytes
      |> LD.(run_parser_string byte_string) in
      let desc' = Fmt.str "decode %s" cbor' in
      T.check T.(result string string) desc' (Ok expected') decoded' in
    List.iter test [
      "", "0x5fff";
      "IETFIETF", "0x5f44494554464449455446ff";
    ]
  end;
  T.test_case "decode byte string (explicit encoding)" `Quick begin fun () ->
    let test (expected', cbor') =
      let decoded' = cbor'
      |> to_bytes
      |> LD.(run_parser_string byte_string) in
      let desc' = Fmt.str "decode %s" cbor' in
      T.check T.(result string string) desc' (Ok expected') decoded' in
    List.iter test [
      "\x01\x02\x03\x04", "0x4401020304";
      "IETF", "0x4449455446";
    ]
  end;
]

let angstrom_tests_utf8_string = [
  T.test_case "encode+decode any utf8 string" `Quick begin fun () ->
    let test data' =
      let roundtrip = data'
      |> LE.encode LE.string
      |> LD.(run_parser_string utf8_string) in
      let desc' = Fmt.str "%s" data' in
      T.check T.(result string string) desc' (Ok data') roundtrip in
    let () = ["hello"; "παλαγάλα"; ""] |> List.iter test in
    (* @todo test longer strings? *)
    let f = BatString.repeat "৯" %> test in
    List.iter f [
      0; 1; 2; 3; 6; 9; 18; 36; 72; 144; 300;
      600; 900; 1800; 3600; 7200; 15000; 30000;
      60000; 120000; 240000; 1000000; 2000000; 10000000;
    ]
  end;
  T.test_case "encode+decode specific utf8 string" `Quick begin fun () ->
    let test data' =
      let roundtrip_good = data'
      |> LE.encode LE.string
      |> LD.(run_parser_string (utf8_string' data')) in
      let roundtrip_bad = data'
      |> LE.encode LE.string
      |> LD.(run_parser_string (utf8_string' "abagaba")) in
      let desc' = Fmt.str "%s" data' in
      T.check T.(result string string) desc' (Ok data') roundtrip_good;
      T.check T.(result string pass) desc' (Error "<any error>") roundtrip_bad;
    in
    let () = ["hello"; "παλαγάλα"; ""] |> List.iter test in
    let f = BatString.repeat "৯" %> test in
    List.iter f [
      0; 1; 2; 3; 6; 9; 18; 36; 72; 144; 300;
      600; 900; 1800; 3600; 7200; 15000; 30000;
      60000; 120000; 240000; 1000000; 2000000; 10000000;
    ]
  end;
  T.test_case "encode+decode utf8 string (indefinite)" `Quick begin fun () ->
    let test (expected', cbor') =
      let decoded' = cbor'
      |> to_bytes
      |> LD.(run_parser_string utf8_string) in
      let desc' = Fmt.str "decode %s" cbor' in
      T.check T.(result string string) desc' (Ok expected') decoded' in
    List.iter test [
      "streaming", "0x7f657374726561646d696e67ff";
      "streamingstreaming", "0x7f657374726561646d696e67657374726561646d696e67ff";
      "", "0x7fff";
    ]
  end;
  T.test_case "decode utf8 string (explicit encoding)" `Quick begin fun () ->
    let test (expected', cbor') =
      let decoded' = cbor'
      |> to_bytes
      |> LD.(run_parser_string utf8_string) in
      let desc' = Fmt.str "decode %s" cbor' in
      T.check T.(result string string) desc' (Ok expected') decoded' in
    List.iter test [
      "a", "0x6161";
      "IETF", "0x6449455446";
    ]
  end;
]

let angstrom_tests_simple = [
  T.test_case "decode simple (explicit encoding)" `Quick begin fun () ->
    let test1 (expected', cbor') =
      let decoded' = cbor'
      |> to_bytes
      |> LD.(run_parser_string simple) in
      let desc' = Fmt.str "%s" cbor' in
      T.check T.(result int string) desc' (Ok expected') decoded' in
    let test2 (expected', cbor') =
      let decoded' = cbor'
      |> to_bytes
      |> LD.(run_parser_string (parse_as 20 @@ simple' expected')) in
      let desc' = Fmt.str "%s" cbor' in
      T.check T.(result int string) desc' (Ok 20) decoded' in
    let test3 (expected', cbor') =
      let decoded' = cbor'
      |> to_bytes
      |> LD.(run_parser_string (simple' expected')) in
      let desc' = Fmt.str "simple %s" cbor' in
      T.check T.(result int string) desc' (Ok expected') decoded' in
    let test x = test1 x; test2 x; test3 x; in
    List.iter test [
      0, "0xe0";
      1, "0xe1";
      19, "0xf3";
      32, "0xf820";
      255, "0xf8ff"
    ];
  end;
]

let angstrom_tests_tuple = [
  T.test_case "decode tuple (explicit encoding)" `Quick begin fun () ->
    let test2 (expected', cbor') =
      let decoded' = cbor'
      |> to_bytes
      |> LD.(run_parser_string (array_tuple2 int bool)) in
      let desc' = Fmt.str "array_tuple2 %s" cbor' in
      T.check T.(result (pair int bool) string) desc' (Ok expected') decoded' in
    let test3 (expected', cbor') =
      let decoded' = cbor'
      |> to_bytes
      |> LD.(run_parser_string (array_tuple3 int bool utf8_string)) in
      let desc' = Fmt.str "array_tuple3 %s" cbor' in
      T.check T.(result (triple int bool string) string) desc' (Ok expected') decoded' in
    let test4 (expected', cbor') =
      let decoded' = cbor'
      |> to_bytes
      |> LD.(run_parser_string (array_tuple4 int bool utf8_string float)) in
      let desc' = Fmt.str "array_tuple4 %s" cbor' in
      T.check T.(result pass string) desc' (Ok expected') decoded' in
    let test5 (expected', cbor') =
      let decoded' = cbor'
      |> to_bytes
      |> LD.(run_parser_string (array_tuple5 int bool utf8_string float int)) in
      let desc' = Fmt.str "array_tuple5 %s" cbor' in
      T.check T.(result pass string) desc' (Ok expected') decoded' in
    test2 ((3, true), "0x9f03f5ff");
    test2 ((3, true), "0x8203f5");
    test3 ((2, false, "hello"), "0x8302f46568656c6c6f");
    test4 ((2, false, "hello", 20.3), "0x8402f46568656c6c6ffb40344ccccccccccd");
    test5 ((2, false, "hello", 20.3, 0), "0x8502f46568656c6c6ffb40344ccccccccccd00");
  end;
]

let angstrom_tests_tag =
  let module M1 = struct
    type u = I of int | F of float
      [@@deriving show]
    let i x = I x
    let f x = F x
  end in [
  T.test_case "decode tag (explicit encoding)" `Quick begin fun () ->
    (* --- tag 0: text-based date/time *)
    let test0 (expected', cbor') =
      let bytes' = cbor' |> to_bytes in
      let decoded' = bytes' |> LD.(run_parser_string tag0) in
      let decoded_generic' = bytes' |> LD.(run_parser_string (tag 0 utf8_string)) in
      let desc' = Fmt.str "tag0 decode %s" cbor' in
      T.check T.(result string string) desc' (Ok expected') decoded';
      T.check T.(result string string) desc' (Ok expected') decoded_generic' in
    (* --- tag 1: epoch-based date/time *)
    let test1 (expected', cbor') =
      let decoded' = cbor'
      |> to_bytes
      |> LD.(run_parser_string (tag1 M1.i M1.f)) in
      let desc' = Fmt.str "tag1 decode %s" cbor' in
      T.check T.(result (of_pp M1.pp_u) string) desc' (Ok expected') decoded' in
    (* --- tag 1: epoch-based date/time, + date/time parser *)
    let test1' (expected', cbor') =
      let f =
        let open Unix in
        let g = gmtime %> fun x -> x.tm_min in
        function
        | M1.I i -> g (float_of_int i)
        | M1.F f -> g f in
      let decoded' = cbor'
      |> to_bytes
      |> LD.(run_parser_string (tag1' f M1.i M1.f)) in
      let desc' = Fmt.str "tag1' decode %s" cbor' in
      T.check T.(result int string) desc' (Ok expected') decoded' in
    (* --- tag 2, tag 3: byte strings *)
    (* --- tag 4, tag 5: int arrays of length 2*)
    let test2_to_5 (expected', testable', cbor', str', parser') =
      let decoded' = cbor'
      |> to_bytes
      |> LD.(run_parser_string parser') in
      let desc' = Fmt.str "%s decode %s" str' cbor' in
      T.check T.(result testable' string) desc' (Ok expected') decoded' in
    let test4_5_with_bignum (expected', cbor', str', parser') =
      let module M = struct
        type x = [`I of int | `BS of string]
        [@@deriving show]
      end in
      let decoded' = cbor'
      |> to_bytes
      |> LD.(run_parser_string parser') in
      let desc' = Fmt.str "%s decode %s" str' cbor' in
      let testable' = T.of_pp M.pp_x in
      T.check T.(result (T.pair testable' testable') string) desc' (Ok expected') decoded' in
    let test_generic (tagnum', parser', expected', testable', cbor') =
      let decoded' = cbor'
      |> to_bytes
      |> LD.(run_parser_string (tag tagnum' parser')) in
      let desc' = Fmt.str "decode %s" cbor' in
      T.check T.(result testable' string) desc' (Ok expected') decoded' in
    List.iter test0 [
      "2013-03-21T20:04:00Z", "0xc074323031332d30332d32315432303a30343a30305a";
      "2013-03-21T20:04:00Z", "0xd80074323031332d30332d32315432303a30343a30305a";
    ];
    List.iter test1 [
      M1.I 1363896240, "0xc11a514b67b0";
      M1.I 1363896240, "0xd8011a514b67b0";
      M1.F 1363896240.5, "0xc1fb41d452d9ec200000";
      M1.F 1363896240.5, "0xd801fb41d452d9ec200000";
    ];
    List.iter test1' [
      4, "0xc11a514b67b0";
      4, "0xc1fb41d452d9ec200000";
    ];
    List.iter test2_to_5 [
      "\x01\x02\x10\x50\xaa\x99\xff", T.string, "0xc24701021050aa99ff", "tag2", LD.tag2;
      "IETF", T.string, "0xc25f4449455446ff", "tag2",  LD.tag2;
      "\x01\x02\x10\x50\xaa\x99\xff", T.string, "0xc34701021050aa99ff", "tag3", LD.tag3;
      "IETF", T.string, "0xc35f4449455446ff", "tag3",  LD.tag3;
    ];
    List.iter test2_to_5 [
      (-2, 27315), T.(pair int int), "0xc48221196ab3", "tag4'", LD.tag4';
      (-2, 27315), T.(pair int int), "0xc58221196ab3", "tag5'", LD.tag5';
    ];
    List.iter test4_5_with_bignum [
      (`I (-2), `I 27315), "0xc48221196ab3", "tag4", LD.tag4;
      (`I (-2), `BS "0a0b0c0d"), "0xc482215f483061306230633064ff", "tag4", LD.tag4;
      (`I (-2), `I 27315), "0xc58221196ab3", "tag4", LD.tag5;
      (`I (-2), `BS "0a0b0c0d"), "0xc582215f483061306230633064ff", "tag4", LD.tag5;
    ];
    test_generic (1, LD.float, 1363896240.5, T.(float epsilon_float), "0xc1fb41d452d9ec200000");
    test_generic (4, LD.(array_tuple2 int int), ((-2), 27315), T.(pair int int), "0xc48221196ab3");
    test_generic (276, LD.byte_string, "mean mr. mustard", T.string, "0xd90114506d65616e206d722e206d757374617264");   
    test_generic (0x99999999, LD.int, 10, T.int, "0xda999999990a");
  end;
]

let angstrom_tests_list = [
  T.test_case "encode+decode float list" `Quick begin fun () ->
    let test data' =
      let roundtrip = data'
      |> LE.encode LE.(list float)
      |> LD.(run_parser_string (array_list float)) in
      let desc' = Fmt.str "%a" pp_list_float_yada_yada data' in
      T.check T.(result (list (float epsilon_float)) string) desc' (Ok data') roundtrip in
    test [
      -13.590; -2734927349234.2834823048234; 0.0; -0.0;
      1.1; 1.10000; 1.150; 12.9; infinity; nan;
    ]
  end;
  T.test_case "encode+decode int list" `Quick begin fun () ->
    let test data' =
      let roundtrip = data'
      |> LE.encode LE.(list int)
      |> LD.(run_parser_string (array_list int)) in
      let desc' = Fmt.str "%a" pp_list_int_yada_yada data' in
      T.check T.(result (list int) string) desc' (Ok data') roundtrip in
    let ints' =
      let ns = BatList.range 1 `To 1000 in
      let f x = BatList.range 1 `To x in
      List.map f ns in
    List.iter test ints'
  end;
  T.test_case "decode int list (indefinite length)" `Quick begin fun () ->
    let test (expected', cbor') =
      let decoded' = cbor'
      |> to_bytes
      |> LD.(run_parser_string (array_list int)) in
      let desc' = Fmt.str "decode %s" cbor' in
      T.check T.(result (list int) string) desc' (Ok expected') decoded' in
    List.iter test [
      [], "0x9fff";
      BatList.range 1 `To 25, "0x9f0102030405060708090a0b0c0d0e0f101112131415161718181819ff";
    ]
  end;
  T.test_case "decode int list list (mixed)" `Quick begin fun () ->
    let test (expected', cbor') =
      let decoded' = cbor'
      |> to_bytes
      |> LD.(run_parser_string (array_list (array_list int))) in
      let desc' = Fmt.str "decode %s" cbor' in
      T.check T.(result (list (list int)) string) desc' (Ok expected') decoded' in
    List.iter test [
      [], "0x9fff";
      [[1; 2; 3]; [4; 5; 6; 7]], "0x9f830102038404050607ff";
      [[1; 2; 3]; [4; 5; 6; 7]], "0x82830102039f04050607ff";
    ]
  end;
]

let angstrom_tests_record = [
  let mouse_data = TT.([
    { age = 10; color = "blue" };
    { age = 20; color = "red" };
    { age = 50; color = "green" };
  ]) in
  let mouse_data_invalid_key = TT.([
    { rage = 10; smoeler = "blue" };
  ]) in
  T.test_case "encode+decode simple record" `Quick begin fun () ->
    let encode' = LE.encode TA.mouse_encoder in
    let decode' = TA.decode_mouse in
    let roundtrip' = encode' %> decode' in
    let test1 data' =
      let desc' = Fmt.str "%a" TT.pp_mouse data' in
      T.check T.(result (of_pp TT.pp_mouse) string) desc' (Ok data') (roundtrip' data') in
    let test2 data' =
      let pp' = TT.pp_mouse_invalid_key in
      let encoded' = LE.encode TA.mouse_invalid_key_encoder data' in
      let desc' = Fmt.str "decode should fail %a" pp' data' in
      let decoded' = decode' encoded' in
      check_result_any_error desc' decoded' in
    let () = List.iter test1 mouse_data in
    let () = List.iter test2 mouse_data_invalid_key in
    ()
  end;
]

let angstrom_tests_map =
  let y_data = [
    "Someting", 10;
    "SomeOtherTing", -12;
  ] in
  let z_data = [
    20.3, 10;
    -10.1, 20;
  ] in [
  T.test_case "encode+decode map" `Quick begin fun () ->
    let roundtrip' encoder' decoder' = LE.encode encoder' %> decoder' in
    let test encoder' decoder' pp' data' =
      let desc' = Fmt.str "%a" pp' data' in
      let round' = roundtrip' encoder' decoder' in
      T.check T.(result (of_pp pp') string) desc' (Ok data') (round' data') in

    let open TA in
    test y_encoder decode_y TT.pp_y y_data;
    test z_encoder decode_z TT.pp_z z_data;
  end;
  T.test_case "decode map (explicit encoding)" `Quick begin fun () ->
    let test1 (expected', cbor') =
      let decoded' : ((float * int) list, string) result = cbor'
      |> to_bytes
      |> LD.(run_parser_string (map (float, int))) in
      let desc' = Fmt.str "decode %s" cbor' in
      T.check T.(result (list (pair (float epsilon_float) int)) string) desc' (Ok expected') decoded' in
    let test2 (expected', cbor') =
      let decoded' : ((string * string) list, string) result = cbor'
      |> to_bytes
      |> LD.(run_parser_string (map (utf8_string, utf8_string))) in
      let desc' = Fmt.str "decode %s" cbor' in
      T.check T.(result (list (pair string string)) string) desc' (Ok expected') decoded' in
    List.iter test1 [
      [20.3, 10; -10.1, 20], "0xa2fb40344ccccccccccd0afbc02433333333333314";
    ];
    List.iter test2 [
      ["red", "rouge"; "blue", "bleu"], "0xa26372656465726f75676564626c756564626c6575";
    ]
  end;
  T.test_case "encode+decode map (indefinite length)" `Quick begin fun () ->
    let test (expected', cbor') =
      let decoded' : ((float * int) list, string) result = cbor'
      |> to_bytes
      |> LD.(run_parser_string (map (float, int))) in
      let desc' = Fmt.str "decode %s" cbor' in
      T.check T.(result (list (pair (float epsilon_float) int)) string) desc' (Ok expected') decoded' in
    List.iter test [
      [20.3, 10; -10.1, 20], "0xbffb40344ccccccccccd0afbc02433333333333314ff";
    ]
  end;
]

(* An example of how to convert a BatMap, embedded in a type constructor, which uses custom types for its keys and values. *)

let angstrom_tests_map_bat_table = [
  let open TT in
  let test_data : TT.routing_table = Routing_table (
    BatMap.empty
    |> BatMap.add (Pubkey "abc") (Ipv4 183472)
    |> BatMap.add (Pubkey "def") (Ipv4 900283)
  ) in
  T.test_case "encode+decode map" `Quick begin fun () ->
    let encode' = LE.encode TA.routing_table_encoder in
    let decode' = TA.decode_routing_table in
    let roundtrip' = encode' %> decode' in
    let pp' = TT.pp_routing_table in
    let test data' =
      let desc' = Fmt.str "%a" pp' data' in
      T.check T.(result (of_pp pp') string) desc' (Ok data') (roundtrip' data') in
    test test_data;
  end
]

let angstrom_tests_null_undefined =
  [
  T.test_case "decode null/undefined (explicit encoding)" `Quick begin fun () ->
    let test (expected', cbor') =
      let decoded' : (TT.spooky list, string) result = cbor'
      |> to_bytes
      |> LD.(run_parser_string (array_list TA.spooky_parser)) in
      let desc' = Fmt.str "decode %s" cbor' in
      T.check T.(result (list (of_pp TT.pp_spooky)) string) desc' (Ok expected') decoded' in
    List.iter test [
      [Null; Null; Null], "0x9ff6f6f6ff";
      [Null; Undefined; Value 10], "0x9ff6f70aff";
    ]
  end;
]

let fst5 (a, _, _, _, _) = a
let snd5 (_, a, _, _, _) = a
let thd5 (_, _, a, _, _) = a
let tu4_5 (_, _, _, a, _) = a
let tu5_5 (_, _, _, _, a) = a

let angstrom_tests_heterogeneous_list =
  let module M1 = struct
    type a = [`I of int | `B of bool | `S of string | `F of float]
    [@@deriving show]
  end in
  let i x = `I x in let b x = `B x in let s x = `S x in let f x = `F x in
  let parser_x = LD.(lift i int <|> lift f float <|> lift b bool <|> lift s utf8_string) in [

  T.test_case "encode+decode heterogeneous 5-array using array_tuple5" `Quick begin fun () ->
    let test data' expected' =
      let roundtrip = data'
      |> LE.encode LE.array
      |> LD.(run_parser_string (array_tuple5 int bool float bool utf8_string)) in
      let desc' = Fmt.str "<desc>" in
      T.check T.(result int string) desc' (Ok (fst5 expected')) (Result.map fst5 roundtrip);
      T.check T.(result bool string) desc' (Ok (snd5 expected')) (Result.map snd5 roundtrip);
      T.check T.(result (float epsilon_float) string) desc' (Ok (thd5 expected')) (Result.map thd5 roundtrip);
      T.check T.(result bool string) desc' (Ok (tu4_5 expected')) (Result.map tu4_5 roundtrip);
      T.check T.(result string string) desc' (Ok (tu5_5 expected')) (Result.map tu5_5 roundtrip) in
    test [`Int 1; `Bool true; `Float 20.3; `Bool false; `Text "hello"] (1, true, 20.3, false, "hello")
  end;

  T.test_case "encode+decode heterogeneous 5-array using arbitrary length parser" `Quick begin fun () ->
    let test data' expected' =
      let roundtrip = data'
      |> LE.encode LE.array
      |> LD.(run_parser_string (array_list parser_x)) in
      let desc' = Fmt.str "<desc>" in
      T.check T.(result (list (of_pp M1.pp_a)) string) desc' (Ok expected') roundtrip in
    test [`Int 1; `Bool true; `Float 20.3; `Bool false; `Text "hello"]
         [`I 1; `B true; `F 20.3; `B false; `S "hello"]
  end;
  T.test_case "decode heterogeneous 5-array using arbitrary length parser, explicit encoding" `Quick begin fun () ->
    let test cbor' expected' =
      let roundtrip = cbor'
      |> LD.(run_parser_string (array_list parser_x)) in
      let desc' = Fmt.str "<desc>" in
      T.check T.(result (list (of_pp M1.pp_a)) string) desc' (Ok expected') roundtrip in
    test (to_bytes "0x8501f5fb40344ccccccccccdf46568656c6c6f")
         [`I 1; `B true; `F 20.3; `B false; `S "hello"]
  end;
]

let angstrom_tests_animal_list = [
  T.test_case "encode+decode animal list" `Quick begin fun () ->
    let test data' =
      let roundtrip = data'
      |> TE.encode
      |> TA.decode_animal_list in
      let desc' = Fmt.str "%a" TTA.pp_animal_list data' in
      check_angstrom_result_ok_and_eq desc' (Ok data') roundtrip in
    List.iter test [data1]
  end;
]

let angstrom_tests_generic = [
  T.test_case "decode generic, explicit encoding" `Quick begin fun () ->
    let test (expected', cbor') =
      let decoded' = cbor'
      |> to_bytes
      |> LD.(run_parser_string decode_generic) in
      let desc' = Fmt.str "decode %s" cbor' in
      T.check T.(result (of_pp LD.pp_generic) string) desc' (Ok expected') decoded' in
    List.iter test [
      `Int (-1578), "0x390629";
      `Array [`Int 4], "0x8104";
      `Array [
        `Array [`Int 4; `Int 5; `Array [`Int 11; `Int 12]];
        `Array [`Int 10];
      ], "0x9f8304059f0b0cff810aff";
      `Bool false, "0xf4";
      `Bool true, "0xf5";
      `Bytes "henlo", "0x4568656e6c6f";
      `Float infinity, "0xf97c00";
      `Float 20.3, "0xfb40344ccccccccccd";
      `Int 71281912398, LE.encode LE.int 71281912398 |> of_bytes;
      `Map [`Text "red", `Text "rouge"; `Text "blue", `Text "bleu"], "0xa26372656465726f75676564626c756564626c6575";
      `Map [`Float 20.3, `Int 10; `Float (-10.1), `Int 20], "0xa2fb40344ccccccccccd0afbc02433333333333314";
      `Map [`Float 20.3, `Int 10; `Float (-10.1),
        `Map [`Text "red", `Text "rouge"; `Text "blue", `Text "bleu"]
      ], "0xa2fb40344ccccccccccd0afbc024333333333333a26372656465726f75676564626c756564626c6575";
      `Null, "0xf6";
      `Simple 32, "0xf820";
      `Tag (0, `Text "2013-03-21T20:04:00Z"), {|0xc074323031332d30332d32315432303a30343a30305a|};
      `Tag (1, `Int 1363896240), {|0xc11a514b67b0|};
      `Tag (1, `Float 1363896240.5), {|0xc1fb41d452d9ec200000|};
      `Text "hello", "0x6568656c6c6f";
      `Undefined, "0xf7";
      `Array [`Null; `Null; `Null], "0x9ff6f6f6ff";
      `Array [`Null; `Undefined; `Int 10], "0x9ff6f70aff";
      `Array [`Array [`Null; `Null; `Null]; `Null; `Null], "0x9f9ff6f6f6fff6f6ff";
      `Array [`Array [`Int 3; `Bool true]], "0x818203f5";
      `Array [`Int 3; `Bool true; `Array [`Int 4]], "0x8303f58104";
      `Array [`Int 1; `Int 2; `Map [`Text "red", `Text "rouge"; `Text "blue", `Text "bleu"]], "0x830102a26372656465726f75676564626c756564626c6575";
      `Map [`Text "red", `Array [`Text "rouge"; `Text "rojo"; `Text "rood"]], "0xa1637265648365726f75676564726f6a6f64726f6f64";
    ]
  end;
  T.test_case "encode generic" `Quick begin fun () ->
    let test (generic', expected') =
      let encoded' = generic'
      |> LE.encode_generic in
      let desc' = Fmt.str "encode %a, expect %s, got %s" LD.pp_generic generic'
        expected' (of_bytes encoded')
      in
      T.check T.string desc' (expected' |> to_bytes) encoded' in
    List.iter test [
      `Int (-1578), "0x390629";
      `Array [`Int 4], "0x8104";
      `Array [
        `Array [`Int 4; `Int 5; `Array [`Int 11; `Int 12]];
        `Array [`Int 10];
      ], "0x82830405820b0c810a";

      `Bool false, "0xf4";
      `Bool true, "0xf5";
      `Bytes "henlo", "0x4568656e6c6f";
      (* infinity has various representations: this is the one the `cbor` library emits *)
      `Float infinity, "0xfb7ff0000000000000";
      `Float 20.3, "0xfb40344ccccccccccd";
      `Int 71281912398, LE.encode LE.int 71281912398 |> of_bytes;
      `Map [`Text "red", `Text "rouge"; `Text "blue", `Text "bleu"], "0xa26372656465726f75676564626c756564626c6575";
      `Map [`Float 20.3, `Int 10; `Float (-10.1), `Int 20], "0xa2fb40344ccccccccccd0afbc02433333333333314";
      `Map [`Float 20.3, `Int 10; `Float (-10.1),
        `Map [`Text "red", `Text "rouge"; `Text "blue", `Text "bleu"]
      ], "0xa2fb40344ccccccccccd0afbc024333333333333a26372656465726f75676564626c756564626c6575";
      `Null, "0xf6";
      `Simple 32, "0xf820";
      `Text "hello", "0x6568656c6c6f";
      `Undefined, "0xf7";
      `Array [`Null; `Null; `Null], "0x83f6f6f6";
      `Array [`Null; `Undefined; `Int 10], "0x83f6f70a";
      `Array [`Array [`Null; `Null; `Null]; `Null; `Null], "0x8383f6f6f6f6f6";
      `Array [`Array [`Int 3; `Bool true]], "0x818203f5";
      `Array [`Int 3; `Bool true; `Array [`Int 4]], "0x8303f58104";
      `Array [`Int 1; `Int 2; `Map [`Text "red", `Text "rouge"; `Text "blue", `Text "bleu"]], "0x830102a26372656465726f75676564626c756564626c6575";
      `Map [`Text "red", `Array [`Text "rouge"; `Text "rojo"; `Text "rood"]], "0xa1637265648365726f75676564726f6a6f64726f6f64";
    ];
  end;
]

let angstrom_tests_generic_pp = [
  T.test_case "generic pretty-print" `Quick begin fun () ->
    let test (generic', expected_rep') =
      let rep' = Fmt.str "%a" LD.pp_generic generic' in
      let desc' = "pp" in
      T.check T.string desc' expected_rep' rep' in
    let () = LD.set_plain true in
    List.iter test [
      `Array [`Array [`Null; `Null; `Null]; `Null; `Null], "[[null, null, null], null, null]";
      `Bytes "hello", "h'68656c6c6f'";
      `Int 10, "10";
      `Map [`Text "red", `Text "rouge"; `Text "blue", `Text "bleu"], {|{"red": "rouge", "blue": "bleu"}|};
      `Map [`Text "re\"d", `Int 10; `Text "bl\\ue", `Int 20], {|{"re\"d": 10, "bl\\ue": 20}|};
      `Tag (0, `Text "2013-03-21T20:04:00Z"), {|0("2013-03-21T20:04:00Z")|};
      `Text {|hello|}, {|"hello"|};
      `Text {|"\|}, {|"\"\\"|};
    ]
  end;
]

let angstrom_tests = []
  @ angstrom_tests_bool
  @ angstrom_tests_byte_string
  @ angstrom_tests_float
  @ angstrom_tests_int
  @ angstrom_tests_list
  @ angstrom_tests_map
  @ angstrom_tests_map_bat_table
  @ angstrom_tests_record
  @ angstrom_tests_simple
  @ angstrom_tests_tuple
  @ angstrom_tests_utf8_string

  (* assumes byte strings have been tested *)
  @ angstrom_tests_tag

  (* assumes lists have been tested *)
  @ angstrom_tests_null_undefined

  @ angstrom_tests_heterogeneous_list
  @ angstrom_tests_animal_list

  @ angstrom_tests_generic
  @ angstrom_tests_generic_pp

let () =
  let () = Fmt.pr "%a" LD.pp_generic (`Array [`Array [`Null; `Null]; `Null]) in
  let () = Fmt.pr "%a" LD.pp_generic (`Array [`Array [`Bytes "henlo"; `Null]; `Null]) in
  T.run "seaboar-test" [
    "angstrom", angstrom_tests;
  ]
