(*
 * SPDX-FileCopyrightText: 2021 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module LD = Seaboar.Decode
module LE = Seaboar.Encode

module TA = Types.Animal
module TT = Types
module TU = Util

let (<|>) = Seaboar.Decode.Angstrom.(<|>)

let mouse_encoder : TT.mouse LE.encoder =
  fun u -> LE.map' [
    "age", LE.int u.age;
    "color", LE.string u.color;
  ]

let mouse_invalid_key_encoder : TT.mouse_invalid_key LE.encoder =
  fun u -> LE.map' [
    "rage", LE.int u.rage;
    "smoeler", LE.string u.smoeler;
  ]

let parser_mouse = LD.map_values_2 TT.mk_mouse (("age", LD.int), ("color", LD.utf8_string))

let y_encoder : TT.y LE.encoder =
  fun tups ->
    let f (s, i) = s, LE.int i in
    let xs = List.map f tups in
    LE.map' xs

let z_encoder : TT.z LE.encoder =
  fun tups ->
    let f (s, i) = LE.float s, LE.int i in
    let xs = List.map f tups in
    LE.map xs

(* Note that while the encoder library makes a distinction between `map'` (to encode
 * tuples with string keys) and `map` (for non-string keys), our decoder
 * library has no such distinction: we just use `map` and provide the
 * appropriate decoder in first position. *)

let parser_y = LD.map (LD.utf8_string, LD.int)
let parser_z = LD.map (LD.float, LD.int)

let parser_pubkey : TT.pubkey LD.t =
  LD.variant_1 "Pubkey" TT.pubkey LD.utf8_string

let parser_ip : TT.ip LD.t =
  LD.variant_1 "Ipv4" TT.ip LD.int

let parser_routing_table : TT.routing_table LD.t =
  let p = LD.map (parser_pubkey, parser_ip) in
  let q = LD.lift TU.table_of_list p in
  LD.variant_1 "RoutingTable" TT.routing_table q

let pubkey_encoder : TT.pubkey LE.encoder = function
  | Pubkey key -> LE.variant "Pubkey" [LE.string key]

let ip_encoder: TT.ip LE.encoder = function
  | Ipv4 ip -> LE.variant "Ipv4" [LE.int ip]

let routing_table_encoder : TT.routing_table LE.encoder = function
  | Routing_table table ->
      let e = TU.table_encoder pubkey_encoder ip_encoder table in
      LE.variant "RoutingTable" [e]

let spooky_parser : TT.spooky LD.t =
  LD.parse_as TT.Null LD.null <|>
  LD.parse_as TT.Undefined LD.undefined <|>
  LD.lift TT.value LD.int

let mk_meta date_created is_shiny =
  let open TA in
  { date_created; is_shiny; }

let parser_meta : TA.meta LD.t =
  LD.map_values_2 mk_meta (("date_created", LD.float), ("is_shiny", LD.bool))

let parser_swim_speed : TA.swim_speed LD.t =
  LD.variant_1 "SwimSpeed" TA.swim_speed LD.int <|>
  LD.variant_1 "SwimSpeeds" TA.swim_speeds (LD.array_list LD.int)

let parser_flight_speed : TA.flight_speed LD.t =
  LD.variant_1 "FlightSpeed" TA.flight_speed LD.int

let parser_brightness : TA.brightness LD.t =
  LD.variant_1 "Brightness" TA.brightness LD.float

let parser_contrast : TA.contrast LD.t =
  LD.variant_1 "Contrast" TA.contrast LD.float

let parser_color1 : TA.color1 LD.t =
  LD.variant_0 "Turquoise" TA.Turquoise <|>
  LD.variant_0 "EmeraldGreen" TA.EmeraldGreen <|>
  LD.variant_0 "RubyRed" TA.RubyRed <|>
  LD.variant_0 "Black" TA.Black

let parser_color2 : TA.color2 LD.t =
  LD.variant_0 "`Aqua" `Aqua <|>
  LD.variant_2 "`Golden" TA.golden (parser_brightness, parser_contrast)

let parser_color : TA.color LD.t =
  LD.variant_1 "Color1" TA.color1 parser_color1 <|>
  LD.variant_1 "Color2" TA.color2 parser_color2

let parser_fly : TA.fly LD.t =
  LD.variant_2 "Fly" TA.fly (parser_flight_speed, parser_color)

let parser_swim : TA.swim LD.t =
  LD.variant_2 "Swim" TA.swim (parser_swim_speed, parser_color)

let parser_wombot : TA.fly LD.t =
  parser_fly

let parser_elephant : TA.elephant LD.t =
  LD.variant_1 "AdultElephant" TA.adult_elephant parser_swim <|>
  LD.variant_1 "BabyElephant" TA.baby_elephant parser_fly

let parser_animal' : TA.animal' LD.t =
  LD.variant_1 "Elephant" TA.elephant parser_elephant <|>
  LD.variant_1 "Wombot" TA.wombot parser_wombot

let parser_animal : TA.animal LD.t =
  LD.variant_2 "Animal" TA.animal (parser_meta, parser_animal')

let parser_animal_list : TA.animal list LD.t =
  LD.array_list parser_animal

let decode = LD.run_parser_string

module M = struct
  let decode_mouse = decode parser_mouse
  let mouse_encoder = mouse_encoder
  let mouse_invalid_key_encoder = mouse_invalid_key_encoder

  let decode_y = decode parser_y
  let y_encoder = y_encoder
  let decode_z = decode parser_z
  let z_encoder = z_encoder

  let decode_routing_table = decode parser_routing_table
  let routing_table_encoder = routing_table_encoder

  let decode_animal_list = decode parser_animal_list

  let spooky_parser = spooky_parser
end
