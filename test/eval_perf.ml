(*
 * SPDX-FileCopyrightText: 2021 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(** This module needs updating before it can provide useful results. *)

module LD = Seaboar.Decode
module LE = Seaboar.Encode

module UT = Util_test

(* test/ *)
module TTA = Types.Animal
module TA = Decode_test.M
module TE = Encode_test

let printf = Format.printf

let yellow, bright_blue, bright_red, underline =
  let open Fmt in
  let () = set_style_renderer stdout `Ansi_tty in
  styled (`Fg `Yellow),
  styled (`Fg (`Hi `Blue)),
  styled (`Fg (`Hi `Red)),
  styled (`Underline)

let print_header s =
  let open Fmt in
  let t = bright_blue string in
  let u = underline string in
  pr "%a %a: %s@." t "٭" u "suite" s

let benchmark_marker =
  let open Fmt in
  let s, t = yellow string, bright_red string in
  let a = "٭" in
  str_like stdout "%a%a%a" s a t a s a

let make_animal_meta () =
    let date' = UT.epoch () in
    let shiny' = UT.random_bool () in
    TTA.mk_meta date' shiny'

let data1 : TTA.animal_list =
    let open TTA in
    [ mk_adult_elephant (make_animal_meta ()) 10 (Color1 Turquoise);
      mk_adult_elephant_multiple_speeds (make_animal_meta ()) [20; 21; 22] (Color1 RubyRed);
      mk_baby_elephant (make_animal_meta ()) 5 (Color1 RubyRed);
      mk_wombot (make_animal_meta ()) 10 ]

let data2 : TTA.animal_list =
    let open TTA in
    [ mk_adult_elephant (make_animal_meta ()) 20 (Color1 Turquoise);
      mk_adult_elephant (make_animal_meta ()) 20 (Color1 RubyRed);
      mk_baby_elephant (make_animal_meta ()) 5 (Color1 RubyRed);
      mk_wombot (make_animal_meta ()) 10 ]

let data_long =
  let open List in
  let xs = concat [data1; data1; data1; data1] in
  let ys = concat [xs; xs; xs; xs] in
  let zs = concat [ys; ys; ys; ys] in
  let as' = concat [zs; zs; zs; zs] in
  let bs = concat [as'; as'; as'; as'] in
  let cs = concat [bs; bs; bs; bs] in
  cs

let default_num_iterations = 50000

(* Taken from the data-encoding repository from Nomadic labs.
   The garbage collection ensures that this won't happen during the benchmark *)
let bench ?(num_iterations = default_num_iterations) name callback =
  Gc.full_major ();
  Gc.compact ();
  let start_time = Sys.time () in
  for _i = 0 to num_iterations - 1 do
    callback ()
  done;
  let end_time = Sys.time () in
  Fmt.pr
    "%s %s took %.3fs for %d iterations.@."
    benchmark_marker
    name
    (end_time -. start_time)
    num_iterations

let bench_animal_list (desc', data', n) =
  let () =
    let s = Fmt.str "decode/encode animal list using various backends: %s (length=%d)" desc' (List.length data') in
    print_header s in

  let bench' = bench ~num_iterations:n in

  let encode_decoder () = TE.encode data' in
  let encoded_decoder = encode_decoder () in
  let decode_angstrom () = TA.decode_animal_list encoded_decoder in

  bench'
  "angstrom-decode"
  (fun () -> ignore @@ decode_angstrom ());
  bench'
  "mattjbray-decoders-encode"
  (fun () -> ignore @@ encode_decoder ());

  Fmt.pr "@."

let bench_various () =
  let () = print_header "compare mattjbray-decoders vs. angstrom on simple types" in

  let ints = BatList.range  0 `To 20 in
  let encode_decoder_ints = List.map (LE.encode LE.int) in
  let encoded_decoder_ints = encode_decoder_ints ints in
  let decode_angstrom_ints = List.map LD.(run_parser_string int) in

  let int_list = BatList.range 1 `To 100 in
  let encoded_decoder_int_list = LE.encode LE.(list int) int_list in

  let float_list = [-184.123; 3.450e10; 0.0; -0.0; infinity; nan] in
  let encoded_decoder_float_list = LE.encode LE.(list float) float_list in

  let decode_angstrom_int_list = LD.(run_parser_string (array_list int)) in
  let decode_angstrom_float_list = LD.(run_parser_string (array_list float)) in

  bench
  ~num_iterations: 10000
  "angstrom-decode-integers"
  (fun () -> ignore @@ decode_angstrom_ints encoded_decoder_ints);

  bench
  ~num_iterations: 10000
  "angstrom-decode-int_list"
  (fun () -> ignore @@ decode_angstrom_int_list encoded_decoder_int_list);

  bench
  ~num_iterations: 10000
  "angstrom-decode-float_list"
  (fun () -> ignore @@ decode_angstrom_float_list encoded_decoder_float_list);

  Fmt.pr "@."

let () =
  let () = bench_various () in
  List.iter bench_animal_list [
    "short list", data1, 50000;
    "long list", data_long, 10;
  ]
