(*
 * SPDX-FileCopyrightText: 2021 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

val write_binary_file : string -> string -> unit
val input_whole_file : in_channel -> string
val epoch : unit -> float
val random_bool : unit -> bool
val str_chunk : int -> string -> string list

module Result : sig 
  val (let+) : ('a, 'b) result -> ('a -> 'c) -> ('c, 'b) result 
  val (and+) : ('a, 'c) result -> ('b, 'c) result -> ('a * 'b, 'c) result 
end
