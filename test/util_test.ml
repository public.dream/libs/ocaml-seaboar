(*
 * SPDX-FileCopyrightText: 2021 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

let write_binary_file out_path' data' =
  let chan = open_out_bin out_path' in
  let () = output_string chan data' in
  close_out chan

let input_whole_file = Stream.of_channel %> BatStream.to_string

let epoch = Unix.gettimeofday
let random_bool = Random.bool
let str_chunk n s = s
  |> BatString.to_list
  |> BatList.ntake n
  |> List.map (BatString.of_list)

module Result = struct
  let (let+) a b = Result.map b a
  let (and+) a b = match a, b with
  | (Ok r1, Ok r2) -> Ok (r1, r2)
  | (Error e, _) -> Error e
  | (_, Error e) -> Error e
end
