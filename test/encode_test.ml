(*
 * SPDX-FileCopyrightText: 2021 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module LE = Seaboar.Encode

module TA = Types.Animal

let meta_encoder : TA.meta LE.encoder =
  fun u -> LE.map' [
    "date_created", LE.float u.date_created;
    "is_shiny", LE.bool u.is_shiny;
  ]

let brightness_encoder : TA.brightness LE.encoder = function
  | Brightness b -> LE.variant "Brightness" [LE.float b]

let contrast_encoder : TA.contrast LE.encoder = function
  | Contrast c -> LE.variant "Contrast" [LE.float c]

let flight_speed_encoder : TA.flight_speed LE.encoder = function
  | FlightSpeed fs -> LE.variant "FlightSpeed" [LE.int fs]

let swim_speed_encoder : TA.swim_speed LE.encoder = function
  | SwimSpeed ss -> LE.variant "SwimSpeed" [LE.int ss]
  | SwimSpeeds sss -> LE.variant "SwimSpeeds" [LE.(list int) sss]

let color1_encoder : TA.color1 LE.encoder = function
  | Turquoise -> LE.variant "Turquoise" []
  | EmeraldGreen -> LE.variant "EmeraldGreen" []
  | RubyRed -> LE.variant "RubyRed" []
  | Black -> LE.variant "Black" []

let color2_encoder : TA.color2 LE.encoder = function
  | `Aqua -> LE.variant "`Aqua" []
  | `Golden (br, ct) -> LE.variant "`Golden" [brightness_encoder br; contrast_encoder ct]

let color_encoder : TA.color LE.encoder = function
  | Color1 c -> LE.variant "Color1" [color1_encoder c]
  | Color2 c -> LE.variant "Color2" [color2_encoder c]

let flight_encoder : TA.fly LE.encoder = function
  | Fly { flight_speed = fs; color = col } -> LE.variant "Fly" [flight_speed_encoder fs; color_encoder col]

let swim_encoder : TA.swim LE.encoder = function
  | Swim (ss, col) -> LE.variant "Swim" [swim_speed_encoder ss; color_encoder col]

let elephant_encoder : TA.elephant LE.encoder = function
  | AdultElephant swim -> LE.variant "AdultElephant" [swim_encoder swim]
  | BabyElephant fly -> LE.variant "BabyElephant" [flight_encoder fly]

let animal'_encoder : TA.animal' LE.encoder = function
  | Elephant e -> LE.variant "Elephant" [elephant_encoder e]
  | Wombot fly -> LE.variant "Wombot" [flight_encoder fly]

let animal_encoder : TA.animal LE.encoder = function
  | Animal (meta', animal') -> LE.variant "Animal" [meta_encoder meta'; animal'_encoder animal']

let animal_list_encoder : TA.animal list LE.encoder = function
  | ans -> LE.list animal_encoder ans

let encode = LE.encode animal_list_encoder
