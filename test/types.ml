(*
 * SPDX-FileCopyrightText: 2021 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(* --- simple record type *)
type mouse =
  { age: int; color: string }
  [@@deriving show]

type mouse_invalid_key =
  { rage: int; smoeler: string }
  [@@deriving show]

(* --- list of tuples for testing map with string key*)
type y = (string * int) list
  [@@deriving show]

(* --- list of tuples for testing map with non-string key*)
type z = (float * int) list
  [@@deriving show]

type pubkey = Pubkey of string
  [@@deriving show]
type ip = Ipv4 of int
  [@@deriving show]
type routing_table = Routing_table of (pubkey, ip) BatMap.t
let routing_table t = Routing_table t
let pubkey s = Pubkey s
let ip i = Ipv4 i

let mk_mouse age color = {age; color}

let pp_routing_table ppf _ =
  let () = Fmt.pf ppf "Table: " in
  ()

type spooky = Null | Undefined | Value of int
  [@@deriving show]
let value i = Value i

module Animal = Types_animal
