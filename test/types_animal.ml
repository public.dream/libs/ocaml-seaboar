(*
 * SPDX-FileCopyrightText: 2021 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(*
 * An animal is an elephant or a wombot. An elephant is an adult elephant or a
 * baby elephant. All animals have a color. Adult elephants can swim at a
 * certain speed or a fixed list of speeds, and baby elephants and wombots can
 * fly at a certain speed.
 *
 * There are also some convenience functions which wrap the constructors and
 * simplify some of the choices.
 *)

type brightness = Brightness of float
  [@@deriving show]

type contrast = Contrast of float
  [@@deriving show]

type color1 = Turquoise | EmeraldGreen | RubyRed | Black
  [@@deriving show]

type color2 = [ `Aqua | `Golden of brightness * contrast ]
  [@@deriving show]

type color = Color1 of color1 | Color2 of color2
  [@@deriving show]

type swim_speed = SwimSpeed of int | SwimSpeeds of int list
  [@@deriving show]

type flight_speed = FlightSpeed of int
  [@@deriving show]

(* --- test inline record syntax *)
type fly = Fly of { flight_speed: flight_speed; color: color; }
  [@@deriving show]

type swim = Swim of swim_speed * color
  [@@deriving show]

type elephant =
  | AdultElephant of swim
  | BabyElephant of fly
  [@@deriving show]

type animal' =
  | Elephant of elephant
  | Wombot of fly
  [@@deriving show]

let brightness b = Brightness b
let contrast c = Contrast c
let golden b c = `Golden (b, c)
let color1 c = Color1 c
let color2 c = Color2 c
let swim_speed ss = SwimSpeed ss
let swim_speeds sss = SwimSpeeds sss
let flight_speed fs = FlightSpeed fs
let fly flight_speed color = Fly { flight_speed; color; }
let swim ss c = Swim (ss, c)
let elephant e = Elephant e
let adult_elephant s = AdultElephant s
let baby_elephant f = BabyElephant f
let wombot w = Wombot w

(* --- meta is a record type for demo/testing purposes *)
type meta = {
  date_created: float;
  is_shiny: bool;
} [@@deriving show]

type animal = Animal of meta * animal'
  [@@deriving show]

type animal_list = animal list
  [@@deriving show]

let animal meta' animal' = Animal (meta', animal')

let adult_elephant' speed' color' = AdultElephant (Swim (speed', color'))
let baby_elephant' speed' color' = BabyElephant (fly speed' color')
let wombot' speed' = Wombot (fly speed' (Color1 Black))

let mk_meta date' shiny' = {
  date_created = date';
  is_shiny = shiny';
}

let mk_adult_elephant meta' speed' color' =
  let ss' = SwimSpeed speed' in
  let e = adult_elephant' ss' color' in
  animal meta' (Elephant e)

let mk_adult_elephant_multiple_speeds meta' speeds' color' =
  let ss' = SwimSpeeds speeds' in
  let e = adult_elephant' ss' color' in
  animal meta' (Elephant e)

let mk_baby_elephant meta' speed' color' =
  let fs' = FlightSpeed speed' in
  let e = baby_elephant' fs' color' in
  animal meta' (Elephant e)

let mk_wombot meta' speed' =
  let fs' = FlightSpeed speed' in
  animal meta' (wombot' fs')

type t = animal
type decode_result = (animal list, string) result
  [@@deriving show]
