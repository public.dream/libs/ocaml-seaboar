module D = Decoders_cbor.Decode

let variant_decoder xs =
  let open D in
  list value >>= function
    | tag :: vals -> begin
        match decode_value string tag with
        | Ok tag' -> begin
            let f (tag'', _) = tag' = tag'' in
            match List.find_all f xs with
            | [] -> fail @@ "Decode error, expected valid variant tag to match spec: " ^ tag'
            | [_, cb] ->
                let fail_decode' e =
                  let () = pp_error Format.str_formatter e in
                  let err' = Buffer.contents Format.stdbuf in
                  let fmt = Fmt.str "Error %s, trying to decode tag %s" err' tag' in
                  fail fmt in
                let fail_arity' vals_recvd =
                  let l_wanted = List.length vals in
                  let l_recvd = List.length vals_recvd in
                  let fmt = Fmt.str "Bad arity for tag (%s), wanted %d values, got %d values" tag' l_wanted l_recvd in
                  fail fmt in
                cb vals (fail_arity', fail_decode')
            | _ -> fail @@ "Decode error, expected correct number of matches in spec for tag: " ^ tag'
        end
        | Error e ->
            let () = pp_error Format.str_formatter e in
            let err' = Buffer.contents Format.stdbuf in
            let fmt = Fmt.str "Error %s: Expected proper encoding for variant" err' in
            fail fmt
    end
    | [] -> fail @@ "Decode error, expected non-empty list"


