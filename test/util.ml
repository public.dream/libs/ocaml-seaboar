(*
 * SPDX-FileCopyrightText: 2021 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module LE = Seaboar.Encode

let list_of_table = BatMap.enum %> BatList.of_enum
let table_of_list xs =
  let f acc (a, b) = BatMap.add a b acc in
  BatList.fold f BatMap.empty xs
let table_map_as_pairs f = list_of_table %> List.map f
(* not efficient *)
let table_encoder kenc venc table =
  let f (key, value) = (kenc key, venc value) in
  let table' = table |> table_map_as_pairs f
  in LE.map table'
