(*
 * SPDX-FileCopyrightText: 2021 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

type value = [
  | `Array of value list
  | `Bool of bool
  | `Bytes of string
  | `Float of float
  | `Int of int
  | `Map of (value * value) list
  | `Null
  | `Simple of int
  | `Tag of int * value
  | `Text of string
  | `Undefined
  ]

type 'a encoder = 'a -> value
type 'a t = 'a encoder

val array : value list -> value
val bool : bool -> value
val bytes : string -> value
val float : float -> value
val int : int -> value
val list : ('a -> value) -> 'a list -> value
val map : (value * value) list -> value
val map' : (string * value) list -> value
val string : string -> value
val null : value
val undefined : value

val variant : string -> value list -> value
val encode : 'a t -> 'a -> string
val encode_generic : value -> string
