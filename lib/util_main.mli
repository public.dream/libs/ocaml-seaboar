(*
 * SPDX-FileCopyrightText: 2021 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

val id : 'a -> 'a
val is_multiple : int -> int -> bool
val both : ('a -> bool) -> ('a -> bool) -> 'a -> bool
val eq : 'a -> 'a -> bool
val lt : 'a -> 'a -> bool
val gt : 'a -> 'a -> bool
val lte : 'a -> 'a -> bool
val gte : 'a -> 'a -> bool
val between :
  ('a -> 'b -> bool) -> ('c -> 'b -> bool) -> 'a -> 'c -> 'b -> bool
val between_excl : 'a -> 'a -> 'a -> bool
val between_incl : 'a -> 'a -> 'a -> bool
val byte_length: int -> int

(* module Result : sig *)
  (* val (let+) : ('a, 'b) result -> ('a -> 'c) -> ('c, 'b) result *)
  (* val (and+) : ('a, 'c) result -> ('b, 'c) result -> ('a * 'b, 'c) result *)
(* end *)

module Angstrom : sig
  type 'a t = 'a Angstrom.t
  val ( let* ) : 'a t -> ('a -> 'b t) -> 'b t
  val ( let+ ) : 'a t -> ('a -> 'b) -> 'b t
  val ( and+ ) : 'a t -> 'b t -> ('a * 'b) t

  val (<|>) : 'a t -> 'a t -> 'a t
  val (<$>) : ('a -> 'b) -> 'a t -> 'b t
  val (<*>) : ('a -> 'b) t -> 'a t -> 'b t

  val ( *> ) : 'a t -> 'b t -> 'b t
  val ( <* ) : 'a t -> 'b t -> 'a t
  val (>>=) : 'a t -> ('a -> 'b t) -> 'b t
  val (<$) : 'a -> 'b t -> 'a t
  val ($>) : 'a t -> 'b -> 'b t
  val (>>|) : 'a t -> ('a -> 'b) -> 'b t

  val void : 'a t -> unit t
  val lift : ('a -> 'b) ->
              'a t ->
              'b t

  val lift2 : ('a -> 'b -> 'c) ->
              'a t ->
              'b t ->
              'c t

  val lift3 : ('a -> 'b -> 'c -> 'd) ->
              'a t ->
              'b t ->
              'c t ->
              'd t

  val lift4 : ('a -> 'b -> 'c -> 'd -> 'e) ->
              'a t ->
              'b t ->
              'c t ->
              'd t ->
              'e t

  val lift5 : ('a -> 'b -> 'c -> 'd -> 'e -> 'f) ->
              'a t ->
              'b t ->
              'c t ->
              'd t ->
              'e t ->
              'f t

  val lift6 : ('a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g) ->
              'a t ->
              'b t ->
              'c t ->
              'd t ->
              'e t ->
              'f t ->
              'g t

  val lift7 : ('a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g -> 'h) ->
              'a t ->
              'b t ->
              'c t ->
              'd t ->
              'e t ->
              'f t ->
              'g t ->
              'h t

  val lift8 : ('a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g -> 'h -> 'i) ->
              'a t ->
              'b t ->
              'c t ->
              'd t ->
              'e t ->
              'f t ->
              'g t ->
              'h t ->
              'i t

  val lift9 : ('a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g -> 'h -> 'i -> 'j) ->
              'a t ->
              'b t ->
              'c t ->
              'd t ->
              'e t ->
              'f t ->
              'g t ->
              'h t ->
              'i t ->
              'j t

  val lift10 : ('a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g -> 'h -> 'i -> 'j -> 'k) ->
              'a t ->
              'b t ->
              'c t ->
              'd t ->
              'e t ->
              'f t ->
              'g t ->
              'h t ->
              'i t ->
              'j t ->
              'k t

end
