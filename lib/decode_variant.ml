(*
 * SPDX-FileCopyrightText: 2021 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module A = Angstrom

module LD = Decode

module UMA = Util_main.Angstrom

let (
  ( let* ), ( *> ), (>>=)
) = A.(
  ( let* ), ( *> ), (>>=)
)

let variant_n n tag p =
  let q =
    let* tag' = LD.utf8_string in
    match tag with
    | x when x = tag' -> p
    | _ ->
      Fmt.str "Bad tag in variant: wanted %s, got %s" tag tag'
      |> A.fail
    in
  let l = n + 1 + 0x80 in
  A.int8 l *> q

let variant_0 tag x =
  let q = A.return x in
  variant_n 0 tag q

let variant_1 tag f p =
  let q = UMA.lift f p in
  variant_n 1 tag q

let variant_2 tag f (p1, p2) =
  let q = UMA.lift2 f p1 p2 in
  variant_n 2 tag q

let variant_3 tag f (p1, p2, p3) =
  let q = UMA.lift3 f p1 p2 p3 in
  variant_n 3 tag q

let variant_4 tag f (p1, p2, p3, p4) =
  let q = UMA.lift4 f p1 p2 p3 p4 in
  variant_n 4 tag q

let variant_5 tag f (p1, p2, p3, p4, p5) =
  let q = UMA.lift5 f p1 p2 p3 p4 p5 in
  variant_n 5 tag q

let variant_6 tag f (p1, p2, p3, p4, p5, p6) =
  let q = UMA.lift6 f p1 p2 p3 p4 p5 p6 in
  variant_n 6 tag q

let variant_7 tag f (p1, p2, p3, p4, p5, p6, p7) =
  let q = UMA.lift7 f p1 p2 p3 p4 p5 p6 p7 in
  variant_n 7 tag q

let variant_8 tag f (p1, p2, p3, p4, p5, p6, p7, p8) =
  let q = UMA.lift8 f p1 p2 p3 p4 p5 p6 p7 p8 in
  variant_n 8 tag q

let variant_9 tag f (p1, p2, p3, p4, p5, p6, p7, p8, p9) =
  let q = UMA.lift9 f p1 p2 p3 p4 p5 p6 p7 p8 p9 in
  variant_n 9 tag q

let variant_10 tag f (p1, p2, p3, p4, p5, p6, p7, p8, p9, p10) =
  let q = UMA.lift10 f p1 p2 p3 p4 p5 p6 p7 p8 p9 p10 in
  variant_n 10 tag q

(* vapy}p0f_lf)hyla, ppjFp0$Fplyla pp0f.ftlj0fnfnll *)

let variant_bind_0 = variant_0

let variant_bind_1 tag f p =
  let q = p >>= f in
  variant_n 1 tag q

let variant_bind_2 tag f (p1, p2) =
  let q =
    let* p1' = p1 in
    let* p2' = p2 in
    f p1' p2' in
  variant_n 2 tag q

let variant_bind_3 tag f (p1, p2, p3) =
  let q =
    let* p1' = p1 in
    let* p2' = p2 in
    let* p3' = p3 in
    f p1' p2' p3' in
  variant_n 3 tag q

let variant_bind_4 tag f (p1, p2, p3, p4) =
  let q =
    let* p1' = p1 in
    let* p2' = p2 in
    let* p3' = p3 in
    let* p4' = p4 in
    f p1' p2' p3' p4' in
  variant_n 4 tag q

let variant_bind_5 tag f (p1, p2, p3, p4, p5) =
  let q =
    let* p1' = p1 in
    let* p2' = p2 in
    let* p3' = p3 in
    let* p4' = p4 in
    let* p5' = p5 in
    f p1' p2' p3' p4' p5' in
  variant_n 5 tag q

let variant_bind_6 tag f (p1, p2, p3, p4, p5, p6) =
  let q =
    let* p1' = p1 in
    let* p2' = p2 in
    let* p3' = p3 in
    let* p4' = p4 in
    let* p5' = p5 in
    let* p6' = p6 in
    f p1' p2' p3' p4' p5' p6' in
  variant_n 6 tag q

let variant_bind_7 tag f (p1, p2, p3, p4, p5, p6, p7) =
  let q =
    let* p1' = p1 in
    let* p2' = p2 in
    let* p3' = p3 in
    let* p4' = p4 in
    let* p5' = p5 in
    let* p6' = p6 in
    let* p7' = p7 in
    f p1' p2' p3' p4' p5' p6' p7' in
  variant_n 7 tag q

let variant_bind_8 tag f (p1, p2, p3, p4, p5, p6, p7, p8) =
  let q =
    let* p1' = p1 in
    let* p2' = p2 in
    let* p3' = p3 in
    let* p4' = p4 in
    let* p5' = p5 in
    let* p6' = p6 in
    let* p7' = p7 in
    let* p8' = p8 in
    f p1' p2' p3' p4' p5' p6' p7' p8' in
  variant_n 8 tag q

let variant_bind_9 tag f (p1, p2, p3, p4, p5, p6, p7, p8, p9) =
  let q =
    let* p1' = p1 in
    let* p2' = p2 in
    let* p3' = p3 in
    let* p4' = p4 in
    let* p5' = p5 in
    let* p6' = p6 in
    let* p7' = p7 in
    let* p8' = p8 in
    let* p9' = p9 in
    f p1' p2' p3' p4' p5' p6' p7' p8' p9' in
  variant_n 9 tag q

let variant_bind_10 tag f (p1, p2, p3, p4, p5, p6, p7, p8, p9, p10) =
  let q =
    let* p1' = p1 in
    let* p2' = p2 in
    let* p3' = p3 in
    let* p4' = p4 in
    let* p5' = p5 in
    let* p6' = p6 in
    let* p7' = p7 in
    let* p8' = p8 in
    let* p9' = p9 in
    let* p10' = p10 in
    f p1' p2' p3' p4' p5' p6' p7' p8' p9' p10' in
  variant_n 10 tag q

(* vapy}pjf_;l$F)hyla, pp/fkyyp0fplf=lllj$F'hylla ppa'j0fn;ll *)
