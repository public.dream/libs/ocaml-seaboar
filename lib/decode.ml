(*
 * SPDX-FileCopyrightText: 2021 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module Angstrom = Angstrom

module Uint32 = Stdint.Uint32
module Uint64 = Stdint.Uint64

module A = Angstrom
module UM = Util_main
module UMA = Util_main.Angstrom

type 'a parser = 'a A.t
type 'a t = 'a parser

module Infix = struct
  let (
    (<|>), (<$>), ( *> ), ( <* ), (>>=), (<$), (>>|)
  ) = UMA.(
    (<|>), (<$>), ( *> ), ( <* ), (>>=), (<$), (>>|)
  )
end

module Let = struct
  let (( let* ), (let+), (and+)) = UMA.(( let* ), (let+), (and+))
end

include Infix
include Let

let fmap = (<$>)
let bind f p = p >>= f
let return, fail, choice = A.(return, fail, choice)

let parse_as r p : 'a A.t  = r <$ p
let parse_value p v =
  let f = function
    | x when x = v -> A.return v
    | _ -> A.fail "fail parse_value" in
  p >>= f

(*
let between_incl_u32 m n x =
  let a = Uint32.(compare x m) in
  let b = Uint32.(compare n x) in
  a <> -1 && b <> -1
*)

let pmarker = A.int8

let uintx n (of_int', shift_left', logor') desc' =
  let cs = A.count n A.any_char in
  let code = Char.code %> of_int' in
  let sl = shift_left' in
  let (||) = logor' in
  let h = function
    | [a; b; c; d] ->
        sl a 24 || sl b 16 || sl c 8 || d
    | [a; b; c; d; e; f; g; h] ->
        sl a 56 || sl b 48 || sl c 40 || sl d 32 ||
        sl e 24 || sl f 16 || sl g 8  || h
    | _ -> failwith (Fmt.str "bad %s" desc') in
  let f = List.map code %> h in
  f <$> cs

let uint32 : Uint32.t A.t =
  uintx 4 Uint32.(of_int, shift_left, logor) "uint32"

let uint64 : Uint64.t A.t =
  uintx 8 Uint64.(of_int, shift_left, logor) "uint64"

(* todo document overflow *)
let uint32_as_int : int A.t =
  Uint32.to_int <$> uint32

(* todo document overflow *)
let uint64_as_int : int A.t =
  Uint64.to_int <$> uint64

(*
 * S|eeeee|ffffffffff = 16 bits
 *)

let half_float =
  let e2 = ( ** ) 2. in
  let h e f =
    let exp = e2 (e -. 15.) in
    exp *. (1. +. (f /. 1024.)) in

  let g e f = match e, f with
  | 0, f -> e2 (-14.) *. (float_of_int f) /. 1024.
  | 31, 0 -> infinity
  | 31, 1 -> nan
  | e, f -> h (float_of_int e) (float_of_int f) in

  let* byte0 = A.any_uint8 in
  let* byte1 = A.any_uint8 in
  let s = (byte0 land 0b10000000) lsr 7 in
  let e = (byte0 land 0b01111100) lsr 2 in
  let f = ((byte0 land 0b00000011) lsl 8) lor byte1 in
  let sign = if s = 1 then -1. else 1. in

  A.return @@ sign *. g e f

let int_pred' p pred =
  let open A in
  let f a = match pred a with
    | true -> return a
    | false -> fail "no" in
  p >>= f

let int8_pred =
  int_pred' A.any_uint8

(*
let negint' c b p om : int A.t =
  let f = (-) c in
  let pint = f <$> p b in
  let pmarker = om |> function
    | Some q -> int8_pred (UM.eq q) *> A.return ()
    | _ -> A.return ()
  in
  pmarker *> pint
*)

let negint_1_to_24 : int A.t =
  let pmarker = int8_pred (UM.between_incl 0x20 0x37) in
  (-) 0x1f <$> pmarker

let negint_25_to_256 : int A.t =
  let p = pmarker 0x38 *> A.any_uint8 in
  (-) (-1) <$> p

let negint_257_to_65536 : int A.t =
  let p = pmarker 0x39 *> A.BE.any_uint16 in
  (-) (-1) <$> p

(* todo document overflow *)
let negint_65537_to_4294967296 : int A.t =
  let p = pmarker 0x3a *> uint32_as_int in
  (-) (-1) <$> p

(* todo document overflow *)
let negint_4294967297_to_max : int A.t =
  let p = pmarker 0x3b *> uint64_as_int in
  (-) (-1) <$> p

let pos_int0 =
  int8_pred (UM.lt 24)
let posint_24_to_255 =
  pmarker 0x18 *> A.any_uint8
let posint_256_to_65535 =
  pmarker 0x19 *> A.BE.any_uint16
(* todo document overflow *)
let posint_65536_to_4294967295 =
  pmarker 0x1a *> uint32_as_int
(* todo document overflow *)
let posint_4294967296_to_max =
  pmarker 0x1b *> uint64_as_int

let int =
  pos_int0                   <|>
  posint_24_to_255           <|>
  posint_256_to_65535        <|>
  posint_65536_to_4294967295 <|>
  posint_4294967296_to_max   <|>

  negint_1_to_24             <|>
  negint_25_to_256           <|>
  negint_257_to_65536        <|>
  negint_65537_to_4294967296 <|>
  negint_4294967297_to_max

let float : float A.t =
  pmarker 0xfb *> A.BE.any_double <|>
  pmarker 0xfa *> A.BE.any_float <|>
  pmarker 0xf9 *> half_float

let int' = parse_value int
let float' = parse_value float

(* todo document overflow *)
(* todo implement 'break' *)
let listlike a0 p : 'a A.t =
  let a1 = a0 + 0x17 in
  let n0, n1, n2, n3 = a1 + 1, a1 + 2, a1 + 3, a1 + 4 in

  let listlike_0 =
    let pmarker' = int8_pred (UM.between_incl a0 a1) in
    let* m = pmarker' in
    p (m - a0) in

  let listlike_n =
    let alt n pcnt' =
      pmarker n *> pcnt' >>= p in
    alt n0 A.any_uint8 <|>
    alt n1 A.BE.any_uint16 <|>
    alt n2 uint32_as_int <|>
    alt n3 uint64_as_int in

  listlike_0 <|> listlike_n

let listlike_indefinite a p =
  let s = A.many p in
  pmarker a *> s <* A.int8 0xff

let listlike_with_indefinite a0 p q =
  listlike a0 p <|> listlike_indefinite (a0 + 0x1f) q

let byte_string : string A.t =
  let definite' =
    let p = A.take in
    listlike 0x40 p in
  let indefinite' =
    let join' = BatString.join "" in
    join' <$> listlike_indefinite 0x5f definite' in
  definite' <|> indefinite'

let utf8_string : string A.t =
  let definite' =
    let p = A.take in
    listlike 0x60 p in
  let indefinite' =
    let join' = BatString.join "" in
    join' <$> listlike_indefinite 0x7f definite' in
  definite' <|> indefinite'

let byte_string' = parse_value byte_string
let utf8_string' = parse_value utf8_string

let array_list p =
  let q cnt = A.count cnt p in
  listlike_with_indefinite 0x80 q p

let array_tuple2, array_tuple3, array_tuple4, array_tuple5 =
  let def m p = pmarker m *> p in
  let indef p = pmarker 0x9f *> p <* pmarker 0xff in

  let tup2 a b = a, b in
  let tup3 a b c = a, b, c in
  let tup4 a b c d = a, b, c, d in
  let tup5 a b c d e = a, b, c, d, e in

  let l2 p0 p1          = UMA.lift2 tup2 p0 p1 in
  let l3 p0 p1 p2       = UMA.lift3 tup3 p0 p1 p2 in
  let l4 p0 p1 p2 p3    = UMA.lift4 tup4 p0 p1 p2 p3 in
  let l5 p0 p1 p2 p3 p4 = UMA.lift5 tup5 p0 p1 p2 p3 p4 in

  let a2' p0 p1 =
    let p = l2 p0 p1 in
    def 0x82 p <|> indef p in
  let a3' p0 p1 p2 =
    let p = l3 p0 p1 p2 in
    def 0x83 p <|> indef p in
  let a4' p0 p1 p2 p3 =
    let p = l4 p0 p1 p2 p3 in
    def 0x84 p <|> indef p in
  let a5' p0 p1 p2 p3 p4  =
    let p = l5 p0 p1 p2 p3 p4 in
    def 0x85 p <|> indef p in

  a2', a3', a4', a5'

let simple =
  let simple1 =
    let pmarker = int8_pred (UM.between_incl 0xe0 0xf3) in
    let f x = x - 0xe0 in
    f <$> pmarker in
  let simple2 =
    let p = int8_pred (UM.between_incl 0x20 0xff) in
    pmarker 0xf8 *> p in
  simple1 <|> simple2

let simple' = function
  | s when s < 0x20 ->
      let f x = x - 0xe0 in
      f <$> pmarker (s + 0xe0)
  | s ->
      pmarker 0xf8 *> pmarker s

(* --- we use 'simple extra' to mean false, true, null, and undefined *)

let undefined = UMA.void (pmarker 0xf7)
let null = UMA.void (pmarker 0xf6)
let false', true', false_as_false, true_as_true =
  let false_as x = parse_as x (pmarker 0xf4) in
  let true_as x = parse_as x (pmarker 0xf5) in
  false_as (), true_as (), false_as false, true_as true

let bool : bool A.t =
  false_as_false <|> true_as_true

let ptag =
  let lookup = function
    | 1 -> 1
    | 2 -> 2
    | 4 -> 3
    | 8 -> 4
    | _ -> failwith "ptag: tag too large" in
  fun n ->
    let tag_small = match n with
    | n when n < 0x18 -> pmarker (n + 0xc0)
    | _ -> A.fail "tag_small; trying tag_big" in
    let tag_big =
      let f l =
        let l' = lookup l in
        let q = A.count l A.any_char  in
        pmarker (l' + 0xd7) *> q in
      f (UM.byte_length n) in
    (ignore <$> tag_small) <|> (ignore <$> tag_big)

let tag n p =
  ptag n *> p

let tag0 : string A.t =
  ptag 0 *> utf8_string

let tag0' p = A.lift p tag0

let tag1 i f : 'a A.t =
  let p = (i <$> int) <|> (f <$> float) in
  ptag 1 *> p

let tag1i =
  ptag 1 *> int

let tag1i' f =
  f <$> tag1i

let tag1f =
  ptag 1 *> float

let tag1f' f =
  f <$> tag1f

let tag1' f i fl = A.lift f (tag1 i fl)

let tag2 : string A.t =
  ptag 2 *> byte_string

let tag2' f = A.lift f tag2

let tag3 : string A.t =
  ptag 3 *> byte_string

let tag3' f = A.lift f tag3

let tag4_5 n =
  let i x = `I x in
  let bs x = `BS x in
  let p0 = array_tuple2 (i <$> int) (i <$> int) in
  let p1 = array_tuple2 (i <$> int) (bs <$> byte_string) in
  ptag n *> (p0 <|> p1)

let tag4 = tag4_5 4
let tag4' = ptag 4 *> array_tuple2 int int

let tag5 = tag4_5 5
let tag5' = ptag 5 *> array_tuple2 int int

let run_parser_string ?(consume=`All) parser' input_string' =
  let consume' = match consume with
  | `All -> A.Consume.All
  | `Prefix -> A.Consume.Prefix in
  A.parse_string ~consume:consume' parser' input_string'

let run_parser_string_exn ?consume parser' input_string' =
  match run_parser_string ?consume parser' input_string' with
  | Ok v      -> v
  | Error msg -> failwith msg
