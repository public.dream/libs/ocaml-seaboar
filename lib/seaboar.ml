(*
 * SPDX-FileCopyrightText: 2021 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module Decode = struct
  include Decode
  include Decode_generic
  include Decode_variant
  include Decode_map
  include Util_main.Angstrom
end
module Encode = struct
  include Encode
end
