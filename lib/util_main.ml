(*
 * SPDX-FileCopyrightText: 2021 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

let id x = x

let is_multiple m n = n mod m = 0

let both p1 p2 x = p1 x && p2 x

let eq = (=)
let lt n m = m < n
let gt n m = m > n
let lte n m = m <= n
let gte n m = m >= n

let between p1 p2 m n =
  let f = p1 m in
  let g = p2 n in
  both f g

let between_excl = fun x -> between gt lt x
let between_incl = fun x -> between gte lte x

let byte_length n =
  let f x =
    let y = x * 8 in
    let m = 2. ** float_of_int y in
    n < int_of_float m in
  List.find f (BatList.range 1 `To 8)

module Angstrom = struct
  type 'a t = 'a Angstrom.t
  let ( let* ), (let+), (and+), (<|>), (<$>), (<*>), ( *> ), ( <* ), (>>=), (>>|) = Angstrom.(
      ( let* ), (let+), (and+), (<|>), (<$>), (<*>), ( *> ), ( <* ), (>>=), (>>|)
  )

  let (<$), ($>) =
    let a x f0 =
      let f _ = x in
      f <$> f0 in
    let b h i = a i h in
    a, b

  let void m = () <$ m
  let lift, lift2, lift3, lift4 = Angstrom.(lift, lift2, lift3, lift4)
  let lift5 f p1 p2 p3 p4 p5                  = lift4 f p1 p2 p3 p4 <*> p5
  let lift6 f p1 p2 p3 p4 p5 p6               = lift5 f p1 p2 p3 p4 p5 <*> p6
  let lift7 f p1 p2 p3 p4 p5 p6 p7            = lift6 f p1 p2 p3 p4 p5 p6 <*> p7
  let lift8 f p1 p2 p3 p4 p5 p6 p7 p8         = lift7 f p1 p2 p3 p4 p5 p6 p7 <*> p8
  let lift9 f p1 p2 p3 p4 p5 p6 p7 p8 p9      = lift8 f p1 p2 p3 p4 p5 p6 p7 p8 <*> p9
  let lift10 f p1 p2 p3 p4 p5 p6 p7 p8 p9 p10 = lift9 f p1 p2 p3 p4 p5 p6 p7 p8 p9 <*> p10
end
