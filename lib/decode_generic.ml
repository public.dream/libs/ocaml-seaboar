module A = Angstrom

module D = Decode
module DM = Decode_map

type t =
	[ `Array of t list
	| `Bool of bool
	| `Bytes of string
	| `Float of float
	| `Int of int
	| `Map of (t * t) list
	| `Null
	| `Simple of int
    | `Tag of (int * t)
	| `Text of string
	| `Undefined ]

module Pp = struct
  let plain = ref false

  let set_plain b =
    plain := b

  (* e.g. "\x61\x62..." -> "6162..." *)
  let of_bytes =
    let f acc x = acc ^ Fmt.str "%02x" (Char.code x) in
    BatString.fold_left f ""

  let lq, rq =
    let f x () = match !plain with
    | true -> "'"
    | _ -> x
    in f "❰", f "❱"

  let quote pp ppf x =
    Fmt.pf ppf "%s%a%s" (lq ()) pp x (rq ())

  let pp_bytes =
    let pp ppf x = Fmt.pf ppf "%s" (of_bytes x) in
    let pq ppf = Fmt.pf ppf "h%a" (quote pp) in
    pq

  let pp_text ppf =
    Fmt.pf ppf "\"%s\"" % BatString.escaped

  let pp_generic =
    let rec f ppf = function
      | `Array xs   ->
          let sep ppf () = Fmt.pf ppf ", " in
          let pp = (Fmt.iter ~sep List.iter f) in
          Fmt.pf ppf "%a" (Fmt.brackets pp) xs
      | `Bool b     -> Fmt.pf ppf "%b" b
      | `Bytes b    -> Fmt.pf ppf "%a" pp_bytes b
      | `Float f    -> Fmt.pf ppf "%f" f
      | `Int i      -> Fmt.pf ppf "%d" i
      | `Map xs     ->
          let sep ppf () = Fmt.pf ppf ", " in
          let pp_pair ppf (k, v) =
            Fmt.pf ppf "%a: %a" f k f v in
          let pp = (Fmt.iter ~sep List.iter pp_pair) in
          Fmt.pf ppf "%a" (Fmt.braces pp) xs
      | `Null       -> Fmt.pf ppf "null"
      | `Simple i   -> Fmt.pf ppf "simple(%d)" i
      | `Tag (t, x) -> Fmt.pf ppf "%d%a" t (Fmt.parens f) x
      | `Text s     -> Fmt.pf ppf "%a" pp_text s
      | `Undefined  -> Fmt.pf ppf "undefined"
    in f
end

let set_plain, pp_generic = Pp.(
  set_plain, pp_generic
)

let decode_generic =
  let generic' pgeneric' =
    let failure_msg = "decode_generic" in

    let array' parray' =
      let failure_msg = "decode_generic -> array'" in
      let q = A.choice ~failure_msg [pgeneric'; parray'] in
      A.lift (fun xs -> `Array xs) (D.array_list q) in

    let map' pmap' =
      let failure_msg = "decode_generic -> map'" in
      let q = A.choice ~failure_msg [pgeneric'; pmap'] in
      A.lift (fun xs -> `Map xs) (DM.map (q, q)) in

    let tag' =
      let failure_msg = "decode_generic -> tag'" in
      A.choice ~failure_msg [
        A.lift (fun s -> `Tag (0, `Text s)) D.tag0;
        A.lift (fun i -> `Tag (1, `Int i)) D.tag1i;
        A.lift (fun f -> `Tag (1, `Float f)) D.tag1f;
      ] in

    A.choice ~failure_msg [
      A.fix array';
      A.lift (fun x -> `Bool x) D.bool;
      A.lift (fun x -> `Bytes x) D.byte_string;
      A.lift (fun x -> `Float x) D.float;
      A.lift (fun x -> `Int x) D.int;
      A.fix map';
      D.parse_as `Null D.null;
      A.lift (fun x -> `Simple x) D.simple;
      tag';
      A.lift (fun x -> `Text x) D.utf8_string;
      D.parse_as `Undefined D.undefined;
  ] in
  A.fix generic'
