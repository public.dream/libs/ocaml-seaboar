(*
 * SPDX-FileCopyrightText: 2021 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

type value = [
  | `Array of value list
  | `Bool of bool
  | `Bytes of string
  | `Float of float
  | `Int of int
  | `Map of (value * value) list
  | `Null
  | `Simple of int
  | `Tag of int * value
  | `Text of string
  | `Undefined
  ]

let array b = `Array b
let bool b = `Bool b
let bytes b = `Bytes b
let float i = `Float i
let int i = `Int i
let string s = `Text s
let null = `Null
let undefined = `Undefined

let list e xs = `Array (List.map e xs)
let map xs = `Map xs
let map' xs =
  let f (k, v) = string k, v in
  let ys = List.map f xs in
  `Map ys

let id x = x

let variant tag xs =
  let ys = string tag :: xs in
  list id ys

let encode_generic =
  let f = CBOR.Simple.encode in
  let rec g =
    let map' xs =
      let f (k, v) acc = (g k, g v) :: acc in
      `Map (List.fold_right f xs []) in
    function
    | `Array xs  -> list g xs
    | `Bool _ as x -> x
    | `Bytes _ as x -> x
    | `Float _ as x -> x
    | `Int _ as x -> x
    | `Null as x -> x
    | `Simple _ as x -> x
    | `Map xs    -> map' xs
    | `Tag _xs   -> failwith "encode_generic: tag encoder is not implemented"
    | `Text _ as x -> x
    | `Undefined -> `Undefined
  in f % g

type 'a encoder = 'a -> value
type 'a t = 'a encoder

let encode e = encode_generic % e
