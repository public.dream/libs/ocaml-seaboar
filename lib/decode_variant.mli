(*
 * SPDX-FileCopyrightText: 2021 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

type 'a t := 'a Decode.t

val variant_0 : string -> 'a -> 'a t
val variant_1 : string -> ('a -> 'b) -> 'a t -> 'b t
val variant_2 : string -> ('a -> 'b -> 'c) -> 'a t * 'b t -> 'c t
val variant_3 : string -> ('a -> 'b -> 'c -> 'd) -> 'a t * 'b t * 'c t -> 'd t
val variant_4 : string -> ('a -> 'b -> 'c -> 'd -> 'e) -> 'a t * 'b t * 'c t * 'd t -> 'e t
val variant_5 : string -> ('a -> 'b -> 'c -> 'd -> 'e -> 'f) -> 'a t * 'b t * 'c t * 'd t * 'e t -> 'f t
val variant_6 : string -> ('a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g) -> 'a t * 'b t * 'c t * 'd t * 'e t * 'f t -> 'g t
val variant_7 : string -> ('a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g -> 'h) -> 'a t * 'b t * 'c t * 'd t * 'e t * 'f t * 'g t -> 'h t
val variant_8 : string -> ('a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g -> 'h -> 'i) -> 'a t * 'b t * 'c t * 'd t * 'e t * 'f t * 'g t * 'h t -> 'i t
val variant_9 : string -> ('a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g -> 'h -> 'i -> 'j) -> 'a t * 'b t * 'c t * 'd t * 'e t * 'f t * 'g t * 'h t * 'i t -> 'j t
val variant_10 : string -> ('a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g -> 'h -> 'i -> 'j -> 'k) -> 'a t * 'b t * 'c t * 'd t * 'e t * 'f t * 'g t * 'h t * 'i t * 'j t -> 'k t

val variant_bind_0 : string -> 'a -> 'a t
val variant_bind_1 : string -> ('a -> 'b t) -> 'a t -> 'b t
val variant_bind_2 : string -> ('a -> 'b -> 'c t) -> ('a t * 'b t) -> 'c t
val variant_bind_3 : string -> ('a -> 'b -> 'c -> 'd t) -> ('a t * 'b t * 'c t) -> 'd t
val variant_bind_4 : string -> ('a -> 'b -> 'c -> 'd -> 'e t) -> ('a t * 'b t * 'c t * 'd t) -> 'e t
val variant_bind_5 : string -> ('a -> 'b -> 'c -> 'd -> 'e -> 'f t) -> ('a t * 'b t * 'c t * 'd t * 'e t) -> 'f t
val variant_bind_6 : string -> ('a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g t) -> ('a t * 'b t * 'c t * 'd t * 'e t * 'f t) -> 'g t
val variant_bind_7 : string -> ('a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g -> 'h t) -> ('a t * 'b t * 'c t * 'd t * 'e t * 'f t * 'g t) -> 'h t
val variant_bind_8 : string -> ('a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g -> 'h -> 'i t) -> ('a t * 'b t * 'c t * 'd t * 'e t * 'f t * 'g t * 'h t) -> 'i t
val variant_bind_9 : string -> ('a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g -> 'h -> 'i -> 'j t) -> ('a t * 'b t * 'c t * 'd t * 'e t * 'f t * 'g t * 'h t * 'i t) -> 'j t
val variant_bind_10 : string -> ('a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g -> 'h -> 'i -> 'j -> 'k t) -> ('a t * 'b t * 'c t * 'd t * 'e t * 'f t * 'g t * 'h t * 'i t * 'j t) -> 'k t

(* yypf_;lf)hhhylla -> 'pa F-hxf)f)hhhylf)i * 'pa tf>f'l *)
