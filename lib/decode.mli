(*
 * SPDX-FileCopyrightText: 2021 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module Angstrom = Angstrom
type 'a parser = 'a Angstrom.t
type 'a t = 'a parser

(** We call this `fmap` to avoid a clash with `map` from `Decode_map` *)
val fmap : ('a -> 'b) -> 'a t -> 'b t
val bind : ('a -> 'b t) -> 'a t -> 'b t
val return : 'a -> 'a t
val fail : string -> 'a t
val choice : ?failure_msg:string -> 'a t list -> 'a t

val parse_as : 'a -> 'b t -> 'a t
val parse_value : 'a t -> 'a -> 'a t

module Infix : sig
  val (<|>) : 'a t -> 'a t -> 'a t
  val (<$>) : ('a -> 'b) -> 'a t -> 'b t
  val ( *> ) : 'a t -> 'b t -> 'b t
  val ( <* ) : 'a t -> 'b t -> 'a t
  val (>>=) : 'a t -> ('a -> 'b t) -> 'b t
  val (<$) : 'a -> 'b t -> 'a t
  val (>>|) : 'a t -> ('a -> 'b) -> 'b t
end

module Let : sig
  val ( let* ) : 'a t -> ('a -> 'b t) -> 'b t
  val (let+) : 'a t -> ('a -> 'b) -> 'b t
  val (and+) : 'a t -> 'b t -> ('a * 'b) t
end

include (module type of Infix)
include (module type of Let)

val uint32 : Stdint.Uint32.t t
val uint64 : Stdint.Uint64.t t
val uint32_as_int : int t
val uint64_as_int : int t

val int8_pred : (int -> bool) -> int t

val int : int t
val int' : int -> int t
val float : float t
val float' : float -> float t

val byte_string : string t
val byte_string' : string -> string t
val utf8_string : string t
val utf8_string' : string -> string t

val simple : int t
val simple' : int -> int t

val bool : bool t
val true' : unit t
val false' : unit t
val true_as_true : bool t
val false_as_false : bool t
val null : unit t
val undefined : unit t

(** Parse CBOR tags and associated date item (0xc6 - 0xdb).

We generally leave it to the user to interpret the resulting data, i.e.
date/time, bignum, fraction, URL, etc.

[tag0'],
[tag0'],
[tag0'],
[tag0'],

The form with ['] for tags 0, 1, 2, and 3 takes a function [f] as its first
parameter and applies it to the parser using a functor map.

E.g. [tag0' f] is equivalent to [lift f tag0] or [f <$> tag0]. *)

val tag : int -> 'a t -> 'a t

(** Parses tag 0 and returns the date/time string as a [string]. To parse the
resulting date/time string you can use [tag0'] and provide your own conversion
function). *)
val tag0 : string t

val tag0' : (string -> 'a) -> 'a t

(** Given functions [f] and [g], parses tag 1 and applies [f] if the data item
is an [int] and [g] if it is a [float]). *)
val tag1 : (int -> 'a) -> (float -> 'a) -> 'a t
val tag1' : ('a -> 'b) -> (int -> 'a) -> (float -> 'a) -> 'b t

(** Parses tag 1 as an [int]. *)
val tag1i : int t
val tag1i' : (int -> 'a) -> 'a t

(** Parses tag 1 as a [float]. *)
val tag1f : float t
val tag1f' : (float -> 'a) -> 'a t

(** Parses tag 2 and returns the resulting bytestring in a [string]. *)
val tag2 : string t
val tag2' : (string -> 'a) -> 'a t

(** Parses tag 3 and returns the resulting bytestring in a [string]. *)
val tag3 : string t
val tag3' : (string -> 'a) -> 'a t

(** Parses tag 4 and returns it as a pair [(m, e)] consisting of the mantissa and
the exponent. [m] is represented as an int tagged with [`I], and [e] either
as a bytestring (stored as a [string] and tagged with [`BS]) or an [int] (tagged
with [`I]). *)
val tag4 : ([> `I of int ] * [> `BS of string | `I of int ]) t

(** Parses tag4 using {!int} for both the mantissa and exponent. Only use this
if you're sure that the exponent can fit in an [int], or else it will fail. *)
val tag4' : (int * int) t

(** Like [tag4], except that it parses tag 5. *)
val tag5 : ([> `I of int ] * [> `BS of string | `I of int ]) t

(** Like [tag4'], except that it parses tag 5. *)
val tag5' : (int * int) t

val array_list : 'a t -> 'a list t
val array_tuple2 : 'a t -> 'b t -> ('a * 'b) t
val array_tuple3 : 'a t -> 'b t -> 'c t -> ('a * 'b * 'c) t
val array_tuple4 : 'a t -> 'b t -> 'c t -> 'd t -> ('a * 'b * 'c * 'd) t
val array_tuple5 : 'a t -> 'b t -> 'c t -> 'd t -> 'e t -> ('a * 'b * 'c * 'd * 'e) t

(* todo make semi-private? *)
val listlike : int -> (int -> 'a t) -> 'a t
val listlike_with_indefinite :
  int ->
  (int -> 'a list t) ->
  'a t -> 'a list t

type consume := [ `All | `Prefix ]
val run_parser_string :
  ?consume:consume ->
  'a t -> string -> ('a, string) result
val run_parser_string_exn : ?consume:consume -> 'a t -> string -> 'a
