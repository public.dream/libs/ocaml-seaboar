module A := Angstrom
module D := Seaboar__.Decode
module DM := Seaboar__.Decode_map
type t =
    [ `Array of t list
    | `Bool of bool
    | `Bytes of string
    | `Float of float
    | `Int of int
    | `Map of (t * t) list
    | `Null
    | `Simple of int
    | `Tag of int * t
    | `Text of string
    | `Undefined ]
val set_plain : bool -> unit
val pp_generic : t Fmt.t
val decode_generic : t A.t
