(*
 * SPDX-FileCopyrightText: 2021 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

type 'a t := 'a Decode.t
type 'a x := string * 'a Decode.t

val map : 'a t * 'b t -> ('a * 'b) list t
val map_values : ('a -> 'b) -> 'a x -> 'b t
val map_values_2 : ('a -> 'b -> 'c) -> 'a x * 'b x -> 'c t
val map_values_3 : ('a -> 'b -> 'c -> 'd) -> 'a x * 'b x * 'c x -> 'd t
val map_values_4 : ('a -> 'b -> 'c -> 'd -> 'e) -> 'a x * 'b x * 'c x * 'd x -> 'e t
val map_values_5 : ('a -> 'b -> 'c -> 'd -> 'e -> 'f) -> 'a x * 'b x * 'c x * 'd x * 'e x -> 'f t
val map_values_6 : ('a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g) -> 'a x * 'b x * 'c x * 'd x * 'e x * 'f x -> 'g t
val map_values_7 : ('a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g -> 'h) -> 'a x * 'b x * 'c x * 'd x * 'e x * 'f x * 'g x -> 'h t
val map_values_8 : ('a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g -> 'h -> 'i) -> 'a x * 'b x * 'c x * 'd x * 'e x * 'f x * 'g x * 'h x -> 'i t
val map_values_9 : ('a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g -> 'h -> 'i -> 'j) -> 'a x * 'b x * 'c x * 'd x * 'e x * 'f x * 'g x * 'h x * 'i x -> 'j t
val map_values_10 : ('a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g -> 'h -> 'i -> 'j -> 'k) -> 'a x * 'b x * 'c x * 'd x * 'e x * 'f x * 'g x * 'h x * 'i x * 'j x -> 'k t
