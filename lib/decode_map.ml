(*
 * SPDX-FileCopyrightText: 2021 alleycat <info@alleycat.cc>
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module A = Angstrom

module LD = Decode
module UM = Util_main
module UMA = Util_main.Angstrom

let (
  ( let* ), ( *> )
) = A.(
  ( let* ), ( *> )
)

let map' n p =
  A.int8 (0xa0 + n) *> p

let map_check_key k vp =
  let* k' = LD.utf8_string in
  match k with
  | x when x = k' -> vp
  | _ -> Fmt.str "Bad key in record: wanted %s, got %s" k k' |> A.fail

let map (kp, vp) =
  let f x y = (x, y) in
  let p = UMA.lift2 f kp vp in
  let q cnt = A.count cnt p in
  LD.listlike_with_indefinite 0xa0 q p

let map_values f (k, vp1) =
  let p = UMA.lift f (map_check_key k vp1) in
  map' 1 p

let map_values_2 f ((k1, vp1), (k2, vp2)) =
  let kv1 = map_check_key k1 vp1 in
  let kv2 = map_check_key k2 vp2 in
  let p = UMA.lift2 f kv1 kv2 in
  map' 2 p

let map_values_3 f ((k1, vp1), (k2, vp2), (k3, vp3)) =
  let kv1 = map_check_key k1 vp1 in
  let kv2 = map_check_key k2 vp2 in
  let kv3 = map_check_key k3 vp3 in
  let p = UMA.lift3 f kv1 kv2 kv3 in
  map' 3 p

let map_values_4 f (
  (k1, vp1), (k2, vp2), (k3, vp3), (k4, vp4)
) =
  let kv1 = map_check_key k1 vp1 in
  let kv2 = map_check_key k2 vp2 in
  let kv3 = map_check_key k3 vp3 in
  let kv4 = map_check_key k4 vp4 in
  let p = UMA.lift4 f kv1 kv2 kv3 kv4 in
  map' 4 p

let map_values_5 f (
  (k1, vp1), (k2, vp2), (k3, vp3), (k4, vp4), (k5, vp5)
) =
  let kv1 = map_check_key k1 vp1 in
  let kv2 = map_check_key k2 vp2 in
  let kv3 = map_check_key k3 vp3 in
  let kv4 = map_check_key k4 vp4 in
  let kv5 = map_check_key k5 vp5 in
  let p = UMA.lift5 f kv1 kv2 kv3 kv4 kv5 in
  map' 5 p

let map_values_6 f (
  (k1, vp1), (k2, vp2), (k3, vp3), (k4, vp4), (k5, vp5), (k6, vp6)
) =
  let kv1 = map_check_key k1 vp1 in
  let kv2 = map_check_key k2 vp2 in
  let kv3 = map_check_key k3 vp3 in
  let kv4 = map_check_key k4 vp4 in
  let kv5 = map_check_key k5 vp5 in
  let kv6 = map_check_key k6 vp6 in
  let p = UMA.lift6 f kv1 kv2 kv3 kv4 kv5 kv6 in
  map' 6 p

let map_values_7 f (
  (k1, vp1), (k2, vp2), (k3, vp3), (k4, vp4), (k5, vp5), (k6, vp6), (k7, vp7)
) =
  let kv1 = map_check_key k1 vp1 in
  let kv2 = map_check_key k2 vp2 in
  let kv3 = map_check_key k3 vp3 in
  let kv4 = map_check_key k4 vp4 in
  let kv5 = map_check_key k5 vp5 in
  let kv6 = map_check_key k6 vp6 in
  let kv7 = map_check_key k7 vp7 in
  let p = UMA.lift7 f kv1 kv2 kv3 kv4 kv5 kv6 kv7 in
  map' 7 p

let map_values_8 f (
  (k1, vp1), (k2, vp2), (k3, vp3), (k4, vp4), (k5, vp5), (k6, vp6), (k7, vp7), (k8, vp8)
) =
  let kv1 = map_check_key k1 vp1 in
  let kv2 = map_check_key k2 vp2 in
  let kv3 = map_check_key k3 vp3 in
  let kv4 = map_check_key k4 vp4 in
  let kv5 = map_check_key k5 vp5 in
  let kv6 = map_check_key k6 vp6 in
  let kv7 = map_check_key k7 vp7 in
  let kv8 = map_check_key k8 vp8 in
  let p = UMA.lift8 f kv1 kv2 kv3 kv4 kv5 kv6 kv7 kv8 in
  map' 8 p

let map_values_9 f (
  (k1, vp1), (k2, vp2), (k3, vp3), (k4, vp4), (k5, vp5), (k6, vp6), (k7, vp7), (k8, vp8), (k9, vp9)
) =
  let kv1 = map_check_key k1 vp1 in
  let kv2 = map_check_key k2 vp2 in
  let kv3 = map_check_key k3 vp3 in
  let kv4 = map_check_key k4 vp4 in
  let kv5 = map_check_key k5 vp5 in
  let kv6 = map_check_key k6 vp6 in
  let kv7 = map_check_key k7 vp7 in
  let kv8 = map_check_key k8 vp8 in
  let kv9 = map_check_key k9 vp9 in
  let p = UMA.lift9 f kv1 kv2 kv3 kv4 kv5 kv6 kv7 kv8 kv9 in
  map' 9 p

let map_values_10 f (
  (k1, vp1), (k2, vp2), (k3, vp3), (k4, vp4), (k5, vp5), (k6, vp6), (k7, vp7), (k8, vp8), (k9, vp9), (k10, vp10)
) =
  let kv1 = map_check_key k1 vp1 in
  let kv2 = map_check_key k2 vp2 in
  let kv3 = map_check_key k3 vp3 in
  let kv4 = map_check_key k4 vp4 in
  let kv5 = map_check_key k5 vp5 in
  let kv6 = map_check_key k6 vp6 in
  let kv7 = map_check_key k7 vp7 in
  let kv8 = map_check_key k8 vp8 in
  let kv9 = map_check_key k9 vp9 in
  let kv10 = map_check_key k10 vp10 in
  let p = UMA.lift10 f kv1 kv2 kv3 kv4 kv5 kv6 kv7 kv8 kv9 kv10 in
  map' 10 p

(* vapy}p0f_;lj$F(y$A, pF(llfpljj/let p
kyypfvlfk;;lfplj0ft;l$Fvlyla kvpj0f'll *)
