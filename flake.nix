{
  description = "Binary encoding/decoding for DREAM";

  outputs = { self, nixpkgs }:
    let
      supportedSystems = [
        "x86_64-linux" "aarch64-linux" "armv7l-linux"
        "x86_64-darwin" "aarch64-darwin"
      ];
      supportedOcamlPackages = [
        "ocamlPackages_4_10"
        "ocamlPackages_4_11"
        "ocamlPackages_4_12"
        "ocamlPackages_4_13"
      ];
      defaultOcamlPackages = "ocamlPackages_4_12";

      forAllOcamlPackages = nixpkgs.lib.genAttrs supportedOcamlPackages;
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
      nixpkgsFor =
        forAllSystems (system:
          import nixpkgs {
            inherit system;
            overlays = [ self.overlay ];
          });
    in
      {
        overlay = final: prev:
          with final;
          let mkOcamlPackages = prevOcamlPackages:
                with prevOcamlPackages;
                let ocamlPackages = {
                      inherit ocaml;
                      inherit findlib;
                      inherit ocamlbuild;
                      inherit opam-file-format;
                      inherit buildDunePackage;

                      cbor =
                        buildDunePackage rec {
                          pname = "cbor";
                          version = "0.4";
                          src = fetchFromGitHub {
                            owner = "ygrek";
                            repo = "ocaml-${pname}";
                            rev = version;
                            sha256 = "0arp17pnaj93aw0pnlr5jlzrkacrzbaapfahvijbxipsaizqc78m";
                          };
                          useDune2 = true;

                          buildInputs = with ocamlPackages; [
                            ocplib-endian
                          ];
                        };

                      decoders =
                        buildDunePackage rec {
                          pname = "decoders";
                          version = "0.5.0";
                          src = fetchFromGitHub {
                            owner = "mattjbray";
                            repo = "ocaml-${pname}";
                            rev = "v${version}";
                            sha256 = "194v2vdhby6i4vgwaa237j6fqfgnxfcn6l1jzr2f11y8m4z23551";
                          };
                          useDune2 = true;

                          buildInputs = with ocamlPackages; [
                          ];
                        };

                      decoders-cbor =
                        buildDunePackage rec {
                          pname = "decoders-cbor";
                          version = "0.5.0";
                          src = fetchFromGitHub {
                            owner = "mattjbray";
                            repo = "ocaml-decoders";
                            rev = "v${version}";
                            sha256 = "194v2vdhby6i4vgwaa237j6fqfgnxfcn6l1jzr2f11y8m4z23551";
                          };
                          useDune2 = true;

                          buildInputs = with ocamlPackages; [
                            cbor
                            decoders
                            ocplib-endian
                          ];
                        };

                      uri =
                        buildDunePackage rec {
                          pname = "uri";
                          version = "4.1.0";
                          src = fetchFromGitHub {
                            owner = "mirage";
                            repo = "ocaml-${pname}";
                            rev = "v${version}";
                            sha256 = "10bf28my1yhj8a2d7bkgbna9j20wq0ghp92k926y29bmjj2qh0l7";
                          };
                          useDune2 = true;

                          buildInputs = with ocamlPackages; [
                            angstrom
                            stringext
                          ];
                        };

                      json-data-encoding =
                        buildDunePackage rec {
                          pname = "json-data-encoding";
                          version = "0.9.1";
                          src = fetchFromGitLab {
                            owner = "nomadic-labs";
                            repo = pname;
                            rev = version;
                            sha256 = "1jrlzs50gqxx41wzq09hbs5j9ghcgl6hx4pfvil4b7cyb2r60acc";
                          };
                          useDune2 = true;

                          buildInputs = with ocamlPackages; [
                            uri
                            angstrom
                            stringext
                          ];
                        };

                      json-data-encoding-bson =
                        buildDunePackage rec {
                          pname = "json-data-encoding-bson";
                          version = "0.9.1";
                          src = fetchFromGitLab {
                            owner = "nomadic-labs";
                            repo = "json-data-encoding";
                            rev = version;
                            sha256 = "1jrlzs50gqxx41wzq09hbs5j9ghcgl6hx4pfvil4b7cyb2r60acc";
                          };
                          useDune2 = true;

                          buildInputs = with ocamlPackages; [
                            json-data-encoding
                            uri
                            angstrom
                            stringext
                            ocplib-endian
                          ];
                        };

                      data-encoding =
                        buildDunePackage rec {
                          pname = "data-encoding";
                          version = "0.3";
                          src = fetchFromGitLab {
                            owner = "nomadic-labs";
                            repo = pname;
                            rev = version;
                            sha256 = "1d58inj9xnma6phpcbdv0ym85pm3raav8h2zp3ybfx1zx41hlig6";
                          };
                          useDune2 = true;

                          buildInputs = with ocamlPackages; [
                            json-data-encoding
                            json-data-encoding-bson
                            uri
                            angstrom
                            stringext
                            ocplib-endian
                            ezjsonm
                            zarith
                          ];
                        };

                      ppx_compose =
                        buildDunePackage rec {
                          pname = "ppx_compose";
                          version = "0.1.0";
                          src = fetchFromGitHub {
                            owner = "paurkedal";
                            repo = pname;
                            rev = "v${version}";
                            sha256 = "19pz4v990w1jmibak6ihxfc1v6if0nzzr0czj8sfj3w27zg51afv";
                          };
                          useDune2 = true;

                          buildInputs = with ocamlPackages; [
                            ocaml-migrate-parsetree
                          ];
                        };

                      ppx_yojson_conv_lib =
                        buildDunePackage rec {
                          pname = "ppx_yojson_conv_lib";
                          version = "0.14.0";
                          src = fetchFromGitHub {
                            owner = "janestreet";
                            repo = pname;
                            rev = "v${version}";
                            sha256 = "12s3xshayy1f8cp9lk6zqwnw60n7cdap55gkksz5w65gdd8bfxmf";
                          };
                          useDune2 = true;

                          buildInputs = with ocamlPackages; [
                            yojson
                          ];
                        };

                      ppx_yojson_conv =
                        buildDunePackage rec {
                          pname = "ppx_yojson_conv";
                          version = "0.14.0";
                          src = fetchFromGitHub {
                            owner = "janestreet";
                            repo = pname;
                            rev = "v${version}";
                            sha256 = "1z64sif8csn0x3l8l0wb81lnz2rpx8q8b4q52yj46gg76i7vr759";
                          };
                          useDune2 = true;

                          buildInputs = with ocamlPackages; [
                            ppx_yojson_conv_lib
                            ppx_js_style
                            ppxlib
                          ];
                        };

                      seaboar =
                        buildDunePackage rec {
                          pname = "seaboar";
                          version = "0.0.1";
                          src = self;

                          useDune2 = true;
                          doCheck = true;

                          nativeBuildInputs = with ocamlPackages; [
                            alcotest
                            odoc
                          ];
                          buildInputs = with ocamlPackages; [
                            angstrom
                            angstrom-lwt-unix
                            batteries
                            cbor
                            data-encoding
                            decoders
                            decoders-cbor
                            ezjsonm
                            json-data-encoding
                            json-data-encoding-bson
                            ocplib-endian
                            ppx_compose
                            ppx_deriving
                            ppx_yojson_conv
                            ppx_yojson_conv_lib
                            ppxlib
                            stdint
                            stringext
                            uri
                            yojson
                            zarith
                          ];
                        };
                    };
                in ocamlPackages;
          in
            let allOcamlPackages =
                  forAllOcamlPackages (ocamlPackages:
                    mkOcamlPackages ocaml-ng.${ocamlPackages});
            in
              allOcamlPackages // {
                ocamlPackages = allOcamlPackages.${defaultOcamlPackages};
              };

        packages =
          forAllSystems (system:
            forAllOcamlPackages (ocamlPackages:
              nixpkgsFor.${system}.${ocamlPackages}));

        defaultPackage =
          forAllSystems (system:
            nixpkgsFor.${system}.ocamlPackages.seaboar);
      };
}
